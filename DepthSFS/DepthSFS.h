#ifndef DEPTHSFS_H
#define DEPTHSFS_H

//Other Library
#include <iostream>
#include <process.h>
#include <math.h>
//OpenCV Library
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
//PCL Library
//#include <pcl/io/pcd_io.h>
//#include <pcl/point_types.h>
//MeshLib Library
#include <MeshLib/core/Mesh/MeshCoreHeaders.h>
//MyMath Library
#include "linequs.h"
#include <splm.h>
#include "levmar.h"
//Matrix Library
#include <Matrix.h>

using namespace MeshLib;
using namespace std;

typedef CBaseMesh<CVertexUV, CEdge, CFace, CHalfEdge> MyMesh;
typedef CIterators<MyMesh> It;

struct ConnectDepth{
	double D = 0;
	double D_top = 0;
	double D_left = 0;
};

struct Coordinate_UV {
	int uv_U = 0;
	int uv_V = 0;
};

struct CameraParam {
	double cameraCx = 0;
	double cameraCy = 0;
	double cameraFx = 0;
	double cameraFy = 0;
};

struct RGBProp {
	int r = 0;
	int g = 0;
	int b = 0;
};

struct NormalProp {
	double nx = 0;
	double ny = 0;
	double nz = 0;
};

struct SHProp {
	double mSH[9] = { 0 };
};

struct NormalPairs {
	NormalProp unitNormal;
	NormalProp initNormal;
};

class DepthSFS
{
public:
	DepthSFS(cv::Mat rgbImage, cv::Mat depthImage, MyMesh* myMesh, CameraParam myCP);
	~DepthSFS();

	void DepthShapeFromShading(cv::Mat& depthImage);

private:
	/*Selecting the face range*/
	void SelectingFaceRange();
	/*Compute normal from depth infomation*/
	void ComputeNormal();
	/*Compute the albedos*/
	void ComputeAlbedos();
	/*Optimizing the depth*/
	void OptimizingDepth();
	/*Filtering illegal point*/
	void SelectOptRange(int startRow, int endRow, int startCol, int endCol);
	/*Inport new Depth*/
	void InportOPTDepth_16UC1(cv::Mat& depthImage);
	/*Inport new Depth*/
	void InportOPTDepth_32FC1(cv::Mat& depthImage);

private:
	/*The size of face pixels*/
	int mFaceSize;
	/*The size of face normal*/
	int mNormalSize;
	/*The size of opt range*/
	int mOPTRangeSize;

	/*Input image's size*/
	int mHeight;
	int mWidth;

	/*Input mesh*/
	MyMesh* mMesh;

	/*The depth(value(ij)) information from depth image*/
	double** mDepths;
	/*The pre depth*/
	double** mPreDepths;
	/*The gray image's intensity(pixels value)*/
	double** mGrayIntensity;
	/*The rgb image's intensity(rgb pixels value)*/
	RGBProp** mRGBIntensity;

	/*The normal(computed by depth information)*/
	NormalProp** mDepthNormal;
	/*The face object's albedos*/
	CPoint** mAlbedos;
	/*Shading*/
	double** mShading;

	/*Face Image mark*/
	bool** mFaceMark;
	/*Depth mark*/
	bool** mNormalMark;
	/*The opt range mark*/
	bool** mOPTRangeMark;

	/*The Camera parameter*/
	CameraParam mCameraParam;
	/*Illumination coefficients*/
	vector<double> LightIllumination;

	///////////////////////
	cv::Mat resultShowThree;
	cv::Mat resultShowGray;
	cv::Mat resultShowDepth;
};

#endif