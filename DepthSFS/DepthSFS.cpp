#include "DepthSFS.h"

#define DIV(n, m)     ((n) / (m))
#define POW2(_value) ((_value) * (_value))

/*
*	Optimizing energy function & it's jac matrix
*/
int Height_OPT;
int Width_OPT;

CameraParam CP_OPT;

vector<int> ParamIndexs_OPT;
vector<double> paramLI_OPT;
vector<double> preDepth_OPT;

CPoint2* paramIij_OPT;
NormalPairs** normalPairs_OPT;
Coordinate_UV* paramUV_OPT;
int** connectedIndex_OPT;

double ComputePosition_Y(Coordinate_UV& Bij_UV, double _depth)
{
	int uv_U = Bij_UV.uv_U;
	int uv_V = Bij_UV.uv_V;

	double Cx = CP_OPT.cameraCx, Cy = CP_OPT.cameraCy,
		Fx = CP_OPT.cameraFx, Fy = CP_OPT.cameraFy;

	return (uv_V - Cy) * _depth / Fy;
}

double ComputeShading(Coordinate_UV& Bij_UV, ConnectDepth& Bij_CD, NormalProp& unitNormal, NormalProp& initNormal)
{
	int uv_U = Bij_UV.uv_U;
	int uv_V = Bij_UV.uv_V;

	double Cx = CP_OPT.cameraCx, Cy = CP_OPT.cameraCy,
		Fx = CP_OPT.cameraFx, Fy = CP_OPT.cameraFy;

	double Dij = Bij_CD.D, Dcj = Bij_CD.D_top, Did = Bij_CD.D_left;

	initNormal.nx = Did * (Dij - Dcj) / Fy;
	initNormal.ny = Dcj * (Dij - Did) / Fx;
	initNormal.nz = initNormal.nx * (Cx - uv_U) / Fx + initNormal.ny * (Cy - uv_V) / Fy - Dcj * Did / (Fx * Fy);

	double nNorm =
		sqrt(POW2(initNormal.nx) + POW2(initNormal.ny) + POW2(initNormal.nz));

	if (nNorm != 0)
	{
		unitNormal.nx = initNormal.nx / nNorm;
		unitNormal.ny = initNormal.ny / nNorm;
		unitNormal.nz = initNormal.nz / nNorm;
	}

	double SphericalHarmonic[9];

	double nX = unitNormal.nx, nY = unitNormal.ny, nZ = unitNormal.nz;

	SphericalHarmonic[0] = 1.0f;
	SphericalHarmonic[1] = nY;
	SphericalHarmonic[2] = nZ;
	SphericalHarmonic[3] = nX;
	SphericalHarmonic[4] = nX * nY;
	SphericalHarmonic[5] = nY * nZ;
	SphericalHarmonic[6] = 2 * nZ * nZ - nX * nX - nY * nY;
	SphericalHarmonic[7] = nZ * nX;
	SphericalHarmonic[8] = nX * nX - nY * nY;

	double LH = 0;
	for (int i = 0; i < 9; i++) 
		LH += SphericalHarmonic[i] * paramLI_OPT[i];

	return LH;
}


void OptimizingDepthFunction(double *p, double *hx, int m, int n, void *adata)
{
	register int currentIndex, k, kPlus1;

	for (k = 0; k < n; k++) {
		kPlus1 = k + 1;
		int ParamIndexs_OPT_Index = DIV(k, 6);

		currentIndex = ParamIndexs_OPT[ParamIndexs_OPT_Index];
		/* a->(i + 1) b->(j + 1) c->(i - 1) d->(j - 1)*/
		int ijIndex = currentIndex;
		int cjIndex = connectedIndex_OPT[currentIndex][0];
		int idIndex = connectedIndex_OPT[currentIndex][1];
		int ajIndex = connectedIndex_OPT[currentIndex][2];
		int ibIndex = connectedIndex_OPT[currentIndex][3];
		int adIndex = connectedIndex_OPT[currentIndex][4];
		int cbIndex = connectedIndex_OPT[currentIndex][5];

		double Dij = p[ijIndex], Dcj = p[cjIndex], Did = p[idIndex],
			Daj = p[ajIndex], Dib = p[ibIndex],
			Dad = p[adIndex], Dcb = p[cbIndex];

		/*f(dij, dcj, did, daj, dad) = ((Bij - Baj) - (Iij - Iaj))*/
		if (kPlus1 % 6 == 1)
		{
			ConnectDepth Bij_CD;
			Bij_CD.D = Dij;
			Bij_CD.D_top = Dcj;
			Bij_CD.D_left = Did;
			ConnectDepth Baj_CD;
			Baj_CD.D = Daj;
			Baj_CD.D_top = Dij;
			Baj_CD.D_left = Dad;

			double Bij = ComputeShading(paramUV_OPT[ijIndex], Bij_CD,
				normalPairs_OPT[currentIndex][0].unitNormal, normalPairs_OPT[currentIndex][0].initNormal);
			double Baj = ComputeShading(paramUV_OPT[ajIndex], Baj_CD,
				normalPairs_OPT[currentIndex][1].unitNormal, normalPairs_OPT[currentIndex][1].initNormal);

			double Iaj = paramIij_OPT[ijIndex][0];
		
			hx[k] = (Bij - Baj) - Iaj;
		}
		/*f(dij, dcj, did, dib, dcb) = ((Bij - Bib) - (Iij - Iib))*/
		else if (kPlus1 % 6 == 2)
		{
			ConnectDepth Bij_CD;
			Bij_CD.D = Dij;
			Bij_CD.D_top = Dcj;
			Bij_CD.D_left = Did;
			ConnectDepth Bib_CD;
			Bib_CD.D = Dib;
			Bib_CD.D_top = Dcb;
			Bib_CD.D_left = Dij;

			double Bij = ComputeShading(paramUV_OPT[ijIndex], Bij_CD,
				normalPairs_OPT[currentIndex][2].unitNormal, normalPairs_OPT[currentIndex][2].initNormal);
			double Bib = ComputeShading(paramUV_OPT[ibIndex], Bib_CD,
				normalPairs_OPT[currentIndex][3].unitNormal, normalPairs_OPT[currentIndex][3].initNormal);

			double Iib = paramIij_OPT[ijIndex][1];

			hx[k] = (Bij - Bib) - Iib;
		}
		/*f(dij, daj, dcj, dib, did) = (Pij.x - 0.25*(Paj.x + Pib.x + Pcj.x + Pid.x)*/
		else if (kPlus1 % 6 == 3)
		{
			int ij_U = paramUV_OPT[ijIndex].uv_U;
			int aj_U = paramUV_OPT[ajIndex].uv_U;
			int ib_U = paramUV_OPT[ibIndex].uv_U;
			int cj_U = paramUV_OPT[cjIndex].uv_U;
			int id_U = paramUV_OPT[idIndex].uv_U;

			double Cx = CP_OPT.cameraCx, Fx = CP_OPT.cameraFx;

			double Pij_X = (ij_U - Cx) * Dij / Fx;
			double Paj_X = (aj_U - Cx) * Daj / Fx;
			double Pib_X = (ib_U - Cx) * Dib / Fx;
			double Pcj_X = (cj_U - Cx) * Dcj / Fx;
			double Pid_X = (id_U - Cx) * Did / Fx;

			hx[k] = Pij_X - 0.25f * (Paj_X + Pib_X + Pcj_X + Pid_X);
		}
		/*f(dij, daj, dcj, dib, did) = (Pij.y - 0.25*(Paj.y + Pib.y + Pcj.y + Pid.y)*/
		else if (kPlus1 % 6 == 4)
		{
			int ij_V = paramUV_OPT[ijIndex].uv_V;
			int aj_V = paramUV_OPT[ajIndex].uv_V;
			int ib_V = paramUV_OPT[ibIndex].uv_V;
			int cj_V = paramUV_OPT[cjIndex].uv_V;
			int id_V = paramUV_OPT[idIndex].uv_V;

			double Cy = CP_OPT.cameraCy, Fy = CP_OPT.cameraFy;

			double Pij_Y = (ij_V - Cy) * Dij / Fy;
			double Paj_Y = (aj_V - Cy) * Daj / Fy;
			double Pib_Y = (ib_V - Cy) * Dib / Fy;
			double Pcj_Y = (cj_V - Cy) * Dcj / Fy;
			double Pid_Y = (id_V - Cy) * Did / Fy;

			hx[k] = Pij_Y - 0.25f * (Paj_Y + Pib_Y + Pcj_Y + Pid_Y);
		}
		/*f(dij, daj, dcj, dib, did) = (Pij.z - 0.25*(Paj.z + Pib.z + Pcj.z + Pid.z)*/
		else if (kPlus1 % 6 == 5)
		{
			double Pij_Z = Dij;
			double Paj_Z = Daj;
			double Pib_Z = Dib;
			double Pcj_Z = Dcj;
			double Pid_Z = Did;

			hx[k] = Pij_Z - 0.25f * (Paj_Z + Pib_Z + Pcj_Z + Pid_Z);
		}
		/*f(dij) = (dij - predij)*/
		else if (kPlus1 % 6 == 0)
		{
			hx[k] = Dij - preDepth_OPT[ijIndex];
		}
	}
	double max = 0;
	for (k = 0; k < n; k++) {
		max += hx[k];
	}
	cout << "current error: " << max << endl;
}

void ComputeDiffShading(Coordinate_UV& Bij_UV, ConnectDepth& Bij_CD, NormalProp& unitNormal, NormalProp& initNormal,
	double& B_diff_Dij, double& B_diff_Dcj, double& B_diff_Did)
{
	int uv_U = Bij_UV.uv_U;
	int uv_V = Bij_UV.uv_V;

	double Cx = CP_OPT.cameraCx, Cy = CP_OPT.cameraCy,
		Fx = CP_OPT.cameraFx, Fy = CP_OPT.cameraFy;

	double Dij = Bij_CD.D, Dcj = Bij_CD.D_top, Did = Bij_CD.D_left;
	/*
	*	initNormal_diff[0]->initNormal_diff_Dij
	*	initNormal_diff[1]->initNormal_diff_Dcj
	*	initNormal_diff[2]->initNormal_diff_Did
	*/
	NormalProp initNormal_diff[3];
	//initNormal_diff_Dij
	initNormal_diff[0].nx = Did / Fy;
	initNormal_diff[0].ny = Dcj / Fx;
	initNormal_diff[0].nz = initNormal_diff[0].nx * (Cx - uv_U) / Fx + initNormal_diff[0].ny * (Cy - uv_V) / Fy;
	//initNormal_diff_Dcj
	initNormal_diff[1].nx = (-1) * Did / Fy;
	initNormal_diff[1].ny = (Dij - Did) / Fx;
	initNormal_diff[1].nz = initNormal_diff[1].nx * (Cx - uv_U) / Fx + initNormal_diff[1].ny * (Cy - uv_V) / Fy - Did / (Fx * Fy);
	//initNormal_diff_Did
	initNormal_diff[2].nx = (Dij - Dcj) / Fy;
	initNormal_diff[2].ny = (-1) * Dcj / Fx;
	initNormal_diff[2].nz = initNormal_diff[2].nx * (Cx - uv_U) / Fx + initNormal_diff[2].ny * (Cy - uv_V) / Fy - Dcj / (Fx * Fy);

	double initNormA = 1 / sqrt(POW2(initNormal.nx) + POW2(initNormal.ny) + POW2(initNormal.nz));
	double initNormB = (-1 / 2) * (initNormA * initNormA * initNormA);
	
	/*
	*	unitNormal_diff[0]->unitNormal_diff_Dij
	*	unitNormal_diff[1]->unitNormal_diff_Dcj
	*	unitNormal_diff[2]->unitNormal_diff_Did
	*/
	NormalProp unitNormal_diff[3];

	for (int i = 0; i < 3; i++)
	{
		double diffNorm = 2 * (initNormal.nx * initNormal_diff[i].nx + initNormal.ny * initNormal_diff[i].ny + initNormal.nz * initNormal_diff[i].nz);
		unitNormal_diff[i].nx = initNormal_diff[i].nx * initNormA + initNormal.nx * initNormB * diffNorm;
		unitNormal_diff[i].ny = initNormal_diff[i].ny * initNormA + initNormal.ny * initNormB * diffNorm;
		unitNormal_diff[i].nz = initNormal_diff[i].nz * initNormA + initNormal.nz * initNormB * diffNorm;
	}

	/*
	*	SH_diff[0]->SH_diff_Dij
	*	SH_diff[1]->SH_diff_Dcj
	*	SH_diff[2]->SH_diff_Did
	*/
	double SH_diff[3][9];

	for (int i = 0; i < 3; i++)
	{
		SH_diff[i][0] = 0;
		SH_diff[i][1] = unitNormal_diff[i].ny;
		SH_diff[i][2] = unitNormal_diff[i].nz;
		SH_diff[i][3] = unitNormal_diff[i].nx;
		SH_diff[i][4] = unitNormal_diff[i].nx * unitNormal.ny + unitNormal.nx * unitNormal_diff[i].ny;
		SH_diff[i][5] = unitNormal_diff[i].ny * unitNormal.nz + unitNormal.ny * unitNormal_diff[i].nz;
		SH_diff[i][6] = 4 * unitNormal.nz * unitNormal_diff[i].nz - 2 * unitNormal.nx * unitNormal_diff[i].nx - 2 * unitNormal.ny * unitNormal_diff[i].ny;
		SH_diff[i][7] = unitNormal_diff[i].nz * unitNormal.nx + unitNormal.nz * unitNormal_diff[i].nx;
		SH_diff[i][8] = 2 * unitNormal.nx * unitNormal_diff[i].nx - 2 * unitNormal.ny * unitNormal_diff[i].ny;
	}

	/*
	*	LH_diff[0]->LH_diff_Dij
	*	LH_diff[1]->LH_diff_Dcj
	*	LH_diff[2]->LH_diff_Did
	*/
	double LH_diff[3] = { 0 };
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			LH_diff[i] += SH_diff[i][j] * paramLI_OPT[j];
		}
	}
	//B_diff_Dij = Kij * LH_diff_Dij;
	B_diff_Dij = LH_diff[0];
	//B_diff_Dcj = Kij * LH_diff_Dcj;
	B_diff_Dcj = LH_diff[1];
	//B_diff_Did = Kij * LH_diff_Did;
	B_diff_Did = LH_diff[2];
}

void OptimizingDepthZPJacCRS(double *p, struct splm_crsm *jac, int m, int n, void *adata)
{
	register int currentIndex, k, kPlus1;
	register int currentJacIndex = 0;

	for (k = 0; k < n; k++) {
		jac->rowptr[k] = currentJacIndex;

		kPlus1 = k + 1;
		int ParamIndexs_OPT_Index = DIV(k, 6);

		currentIndex = ParamIndexs_OPT[ParamIndexs_OPT_Index];

		/* a->(i + 1) b->(j + 1) c->(i - 1) d->(j - 1)*/
		int ijIndex = currentIndex;
		int cjIndex = connectedIndex_OPT[currentIndex][0];
		int idIndex = connectedIndex_OPT[currentIndex][1];
		int ajIndex = connectedIndex_OPT[currentIndex][2];
		int ibIndex = connectedIndex_OPT[currentIndex][3];
		int adIndex = connectedIndex_OPT[currentIndex][4];
		int cbIndex = connectedIndex_OPT[currentIndex][5];
		/*f(dij, dcj, did, daj, dad) = ((Bij - Baj) - (Iij - Iaj))*/
		if (kPlus1 % 6 == 1)
		{
			/*Diff(f(dij, dcj, did, daj, dad), dij)*/
			jac->colidx[currentJacIndex] = ijIndex;
			currentJacIndex++;
			/*Diff(f(dij, dcj, did, daj, dad), dcj)*/
			jac->colidx[currentJacIndex] = cjIndex;
			currentJacIndex++;
			/*Diff(f(dij, dcj, did, daj, dad), did)*/
			jac->colidx[currentJacIndex] = idIndex;
			currentJacIndex++;
			/*Diff(f(dij, dcj, did, daj, dad), daj)*/
			jac->colidx[currentJacIndex] = ajIndex;
			currentJacIndex++;
			/*Diff(f(dij, dcj, did, daj, dad), dad)*/
			jac->colidx[currentJacIndex] = adIndex;
			currentJacIndex++;
		}
		/*f(dij, dcj, did, dib, dcb) = ((Bij - Bib) - (Iij - Iib))*/
		else if (kPlus1 % 6 == 2)
		{
			/*Diff(f(dij, dcj, did, dib, dcb), dij)*/
			jac->colidx[currentJacIndex] = ijIndex;
			currentJacIndex++;
			/*Diff(f(dij, dcj, did, dib, dcb), dcj)*/
			jac->colidx[currentJacIndex] = cjIndex;
			currentJacIndex++;
			/*Diff(f(dij, dcj, did, dib, dcb), did)*/
			jac->colidx[currentJacIndex] = idIndex;
			currentJacIndex++;
			/*Diff(f(dij, dcj, did, dib, dcb), dib)*/
			jac->colidx[currentJacIndex] = ibIndex;
			currentJacIndex++;
			/*Diff(f(dij, dcj, did, dib, dcb), dcb)*/
			jac->colidx[currentJacIndex] = cbIndex;
			currentJacIndex++;
		}
		/*f(dij, daj, dcj, dib, did) = (Pij.x - 0.25*(Paj.x + Pib.x + Pcj.x + Pid.x)*/
		else if (kPlus1 % 6 == 3)
		{
			/*Diff(f(dij, daj, dcj, dib, did), dij)*/
			jac->colidx[currentJacIndex] = ijIndex;
			currentJacIndex++;
			/*Diff(f(dij, daj, dcj, dib, did), daj)*/
			jac->colidx[currentJacIndex] = ajIndex;
			currentJacIndex++;
			/*Diff(f(dij, daj, dcj, dib, did), dib)*/
			jac->colidx[currentJacIndex] = ibIndex;
			currentJacIndex++;
			/*Diff(f(dij, daj, dcj, dib, did), dcj)*/
			jac->colidx[currentJacIndex] = cjIndex;
			currentJacIndex++;
			/*Diff(f(dij, daj, dcj, dib, did), did)*/
			jac->colidx[currentJacIndex] = idIndex;
			currentJacIndex++;
		}
		/*f(dij, daj, dcj, dib, did) = (Pij.y - 0.25*(Paj.y + Pib.y + Pcj.y + Pid.y)*/
		else if (kPlus1 % 6 == 4)
		{
			/*Diff(f(dij, daj, dcj, dib, did), dij)*/
			jac->colidx[currentJacIndex] = ijIndex;
			currentJacIndex++;
			/*Diff(f(dij, daj, dcj, dib, did), daj)*/
			jac->colidx[currentJacIndex] = ajIndex;
			currentJacIndex++;
			/*Diff(f(dij, daj, dcj, dib, did), dib)*/
			jac->colidx[currentJacIndex] = ibIndex;
			currentJacIndex++;
			/*Diff(f(dij, daj, dcj, dib, did), dcj)*/
			jac->colidx[currentJacIndex] = cjIndex;
			currentJacIndex++;
			/*Diff(f(dij, daj, dcj, dib, did), did)*/
			jac->colidx[currentJacIndex] = idIndex;
			currentJacIndex++;
		}
		/*f(dij, daj, dcj, dib, did) = (Pij.z - 0.25*(Paj.z + Pib.z + Pcj.z + Pid.z)*/
		else if (kPlus1 % 6 == 5)
		{
			/*Diff(f(dij, daj, dcj, dib, did), dij)*/
			jac->colidx[currentJacIndex] = ijIndex;
			currentJacIndex++;
			/*Diff(f(dij, daj, dcj, dib, did), daj)*/
			jac->colidx[currentJacIndex] = ajIndex;
			currentJacIndex++;
			/*Diff(f(dij, daj, dcj, dib, did), dib)*/
			jac->colidx[currentJacIndex] = ibIndex;
			currentJacIndex++;
			/*Diff(f(dij, daj, dcj, dib, did), dcj)*/
			jac->colidx[currentJacIndex] = cjIndex;
			currentJacIndex++;
			/*Diff(f(dij, daj, dcj, dib, did), did)*/
			jac->colidx[currentJacIndex] = idIndex;
			currentJacIndex++;
		}
		/*f(dij) = (dij - predij)*/
		else if (kPlus1 % 6 == 0)
		{
			/*Diff(f(dij), dij)*/
			jac->colidx[currentJacIndex] = ijIndex;
			currentJacIndex++;
		}
	}
	jac->rowptr[n] = currentJacIndex;
}

void OptimizingDepthAnjacCRS(double *p, struct splm_crsm *jac, int m, int n, void *adata)
{
	register int currentIndex, k, kPlus1;
	register int currentJacIndex = 0;

	for (k = 0; k < n; k++) {
		jac->rowptr[k] = currentJacIndex;

		kPlus1 = k + 1;
		int ParamIndexs_OPT_Index = DIV(k, 2);

		currentIndex = ParamIndexs_OPT[ParamIndexs_OPT_Index];

		/* a->(i + 1) b->(j + 1) c->(i - 1) d->(j - 1)*/
		int ijIndex = currentIndex;
		int cjIndex = connectedIndex_OPT[currentIndex][0];
		int idIndex = connectedIndex_OPT[currentIndex][1];
		int ajIndex = connectedIndex_OPT[currentIndex][2];
		int ibIndex = connectedIndex_OPT[currentIndex][3];
		int adIndex = connectedIndex_OPT[currentIndex][4];
		int cbIndex = connectedIndex_OPT[currentIndex][5];

		double Dij = p[ijIndex], Dcj = p[cjIndex], Did = p[idIndex],
			Daj = p[ajIndex], Dib = p[ibIndex],
			Dad = p[adIndex], Dcb = p[cbIndex];

		/*f(dij, dcj, did, daj, dad) = ((Bij - Baj) - (Iij - Iaj))*/
		if (kPlus1 % 2 == 1)
		{
			/*Compute Diff(Shading_Bij)*/
			ConnectDepth Bij_CD;
			Bij_CD.D = Dij;
			Bij_CD.D_top = Dcj;
			Bij_CD.D_left = Did;

			double Bij_diff_Dij = 0;
			double Bij_diff_Dcj = 0;
			double Bij_diff_Did = 0;

			double Bij_diff_Daj = 0;
			double Bij_diff_Dad = 0;

			ComputeDiffShading(paramUV_OPT[ijIndex], Bij_CD,
				normalPairs_OPT[currentIndex][0].unitNormal, normalPairs_OPT[currentIndex][0].initNormal,
				Bij_diff_Dij, Bij_diff_Dcj, Bij_diff_Did);

			/*Compute Diff(Shading_Baj)*/
			ConnectDepth Baj_CD;
			Baj_CD.D = Daj;
			Baj_CD.D_top = Dij;
			Baj_CD.D_left = Dad;

			double Baj_diff_Daj = 0;
			double Baj_diff_Dij = 0;
			double Baj_diff_Dad = 0;

			double Baj_diff_Dcj = 0;
			double Baj_diff_Did = 0;

			ComputeDiffShading(paramUV_OPT[ajIndex], Baj_CD,
				normalPairs_OPT[currentIndex][1].unitNormal, normalPairs_OPT[currentIndex][1].initNormal,
				Baj_diff_Daj, Baj_diff_Dij, Baj_diff_Dad);

			/*Diff(f(dij, dcj, did, daj, dad), dij)*/
			double Fij_diff_dij = Bij_diff_Dij - Baj_diff_Dij;
			jac->val[currentJacIndex] = Fij_diff_dij;
			jac->colidx[currentJacIndex] = ijIndex;
			currentJacIndex++;

			/*Diff(f(dij, dcj, did, daj, dad), dcj)*/
			double Fij_diff_dcj = Bij_diff_Dcj - Baj_diff_Dcj;
			jac->val[currentJacIndex] = Fij_diff_dcj;
			jac->colidx[currentJacIndex] = cjIndex;
			currentJacIndex++;

			/*Diff(f(dij, dcj, did, daj, dad), did)*/
			double Fij_diff_did = Bij_diff_Did - Baj_diff_Did;
			jac->val[currentJacIndex] = Fij_diff_did;
			jac->colidx[currentJacIndex] = idIndex;
			currentJacIndex++;

			/*Diff(f(dij, dcj, did, daj, dad), daj)*/
			double Fij_diff_daj = Bij_diff_Daj - Baj_diff_Daj;
			jac->val[currentJacIndex] = Fij_diff_daj;
			jac->colidx[currentJacIndex] = ajIndex;
			currentJacIndex++;

			/*Diff(f(dij, dcj, did, daj, dad), dad)*/
			double Fij_diff_dad = Bij_diff_Dad - Baj_diff_Dad;
			jac->val[currentJacIndex] = Fij_diff_dad;
			jac->colidx[currentJacIndex] = adIndex;
			currentJacIndex++;
		}

		/*f(dij, dcj, did, dib, dcb) = ((Bij - Bib) - (Iij - Iib))*/
		else if (kPlus1 % 2 == 0)
		{
			/*Compute Diff(Shading_Bij)*/
			ConnectDepth Bij_CD;
			Bij_CD.D = Dij;
			Bij_CD.D_top = Dcj;
			Bij_CD.D_left = Did;

			double Bij_diff_dij = 0;
			double Bij_diff_dcj = 0;
			double Bij_diff_did = 0;

			double Bij_diff_dib = 0;
			double Bij_diff_dcb = 0;

			ComputeDiffShading(paramUV_OPT[ijIndex], Bij_CD,
				normalPairs_OPT[currentIndex][2].unitNormal, normalPairs_OPT[currentIndex][2].initNormal,
				Bij_diff_dij, Bij_diff_dcj, Bij_diff_did);

			/*Compute Diff(Shading_Bib)*/
			ConnectDepth Bib_CD;
			Bib_CD.D = Dib;
			Bib_CD.D_top = Dcb;
			Bib_CD.D_left = Dij;

			double Bib_diff_dib = 0;
			double Bib_diff_dcb = 0;
			double Bib_diff_dij = 0;

			double Bib_diff_dcj = 0;
			double Bib_diff_did = 0;

			ComputeDiffShading(paramUV_OPT[ibIndex], Bib_CD,
				normalPairs_OPT[currentIndex][3].unitNormal, normalPairs_OPT[currentIndex][3].initNormal,
				Bib_diff_dib, Bib_diff_dcb, Bib_diff_dij);

			/*Diff(f(dij, dcj, did, dib, dcb), dij)*/
			double Fij_diff_dij = Bij_diff_dij - Bib_diff_dij;
			jac->val[currentJacIndex] = Fij_diff_dij;
			jac->colidx[currentJacIndex] = ijIndex;
			currentJacIndex++;
			/*Diff(f(dij, dcj, did, dib, dcb), dcj)*/

			double Fij_diff_dcj = Bij_diff_dcj - Bib_diff_dcj;
			jac->val[currentJacIndex] = Fij_diff_dcj;
			jac->colidx[currentJacIndex] = cjIndex;
			currentJacIndex++;
			/*Diff(f(dij, dcj, did, dib, dcb), did)*/

			double Fij_diff_did = Bij_diff_did - Bib_diff_did;
			jac->val[currentJacIndex] = Fij_diff_did;
			jac->colidx[currentJacIndex] = idIndex;
			currentJacIndex++;
			/*Diff(f(dij, dcj, did, dib, dcb), dib)*/

			double Fij_diff_dib = Bij_diff_dib - Bib_diff_dib;
			jac->val[currentJacIndex] = Fij_diff_dib;
			jac->colidx[currentJacIndex] = ibIndex;
			currentJacIndex++;
			/*Diff(f(dij, dcj, did, dib, dcb), dcb)*/

			double Fij_diff_dcb = Bij_diff_dcb - Bib_diff_dcb;
			jac->val[currentJacIndex] = Fij_diff_dcb;
			jac->colidx[currentJacIndex] = cbIndex;
			currentJacIndex++;
		}
	}
	jac->rowptr[n] = currentJacIndex;
}

DepthSFS::DepthSFS(cv::Mat rgbImage, cv::Mat depthImage, MyMesh* myMesh, CameraParam myCP)
{
	if (rgbImage.channels() != 3)
	{
		printf("The current image isn't a RGB image!\n");
		return;
	}

	cv::Mat mGrayImg;

	cv::cvtColor(rgbImage, mGrayImg, CV_RGB2GRAY);

	resultShowGray = mGrayImg.clone();
	resultShowDepth = mGrayImg.clone();
	resultShowThree = rgbImage.clone();

	mHeight = rgbImage.rows;
	mWidth = rgbImage.cols;

	/*Set Mesh*/
	mMesh = myMesh;
	/*Set Camera Parameter*/
	mCameraParam = myCP;

	mFaceSize = 0;

	/*Alloc memory*/
	//Depth
	mDepths = new double*[mHeight];
	//Pre Depth
	mPreDepths = new double*[mHeight];
	//Gray intensity
	mGrayIntensity = new double*[mHeight];
	//RGB intensity
	mRGBIntensity = new RGBProp*[mHeight];

	//Normal
	mDepthNormal = new NormalProp*[mHeight];
	//Albedos
	mAlbedos = new CPoint*[mHeight];
	//Shading
	mShading = new double*[mHeight];

	//Face image mark
	mFaceMark = new bool*[mHeight];
	//Normal mark
	mNormalMark = new bool*[mHeight];
	//OPT Range mark
	mOPTRangeMark = new bool*[mHeight];
	
	for (int i = 0; i < mHeight; i++)
	{
		//Depth
		mDepths[i] = new double[mWidth];
		//Pre Depth
		mPreDepths[i] = new double[mWidth];
		//Gray intensity
		mGrayIntensity[i] = new double[mWidth];
		//RGB intensity
		mRGBIntensity[i] = new RGBProp[mWidth];
		
		//Normal
		mDepthNormal[i] = new NormalProp[mWidth];
		//Albedos
		mAlbedos[i] = new CPoint[mWidth];
		//Shading
		mShading[i] = new double[mWidth];

		//Face image mark
		mFaceMark[i] = new bool[mWidth];
		//Normal mark
		mNormalMark[i] = new bool[mWidth];
		//OPT Range mark
		mOPTRangeMark[i] = new bool[mWidth];
	}
	/*Init*/
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			//Init depth
			ushort d = depthImage.ptr<ushort>(i)[j];
			mDepths[i][j] = (double)d;
			//Init gray image's intensity
			mGrayIntensity[i][j] = mGrayImg.ptr<uchar>(i)[j];
			//Init rgb image's intensity
			int index = j * 3;
			mRGBIntensity[i][j].r = rgbImage.ptr<uchar>(i)[index];
			mRGBIntensity[i][j].g = rgbImage.ptr<uchar>(i)[index + 1];
			mRGBIntensity[i][j].b = rgbImage.ptr<uchar>(i)[index + 2];

			//Init albedos
			mAlbedos[i][j][0] = mAlbedos[i][j][1] = mAlbedos[i][j][2] = 0;
			//Init shading
			mShading[i][j] = 0;

			//Init image's face mark
			mFaceMark[i][j] = false;
			//Init image's normal mark
			mNormalMark[i][j] = false;
			//Init image's opt range mark
			mOPTRangeMark[i][j] = false;

			resultShowDepth.ptr<uchar>(i)[j] = 0;
		}
	}
}

DepthSFS::~DepthSFS()
{
	for (int i = 0; i < mHeight; i++)
	{
		delete[] mDepths[i];
		delete[] mGrayIntensity[i];
		delete[] mRGBIntensity[i];
		
		delete[] mDepthNormal[i];
		delete[] mAlbedos[i];
		delete[] mShading[i];

		delete[] mFaceMark[i];
		delete[] mNormalMark[i];
		delete[] mOPTRangeMark[i];
	}
	delete[] mDepths;
	delete[] mGrayIntensity;
	delete[] mRGBIntensity;
	
	delete[] mDepthNormal;
	delete[] mAlbedos;
	delete[] mShading;

	delete[] mFaceMark;
	delete[] mNormalMark;
	delete[] mOPTRangeMark;
}

void DepthSFS::DepthShapeFromShading(cv::Mat& depthImage)
{
	SelectingFaceRange();

	ComputeNormal();

	ComputeAlbedos();

	///*OptimazingFace*/
	//SelectOptRange(331, 660, 450, 760);
	//OptimizingDepth();

	/*Optimazing nose*/
	SelectOptRange(481, 600, 510, 680);
	OptimizingDepth();

	/*Optimazing mouth*/
	SelectOptRange(601, 710, 500, 710);
	OptimizingDepth();

	/*Optimazing left eye*/
	SelectOptRange(331, 480, 420, 600);
	OptimizingDepth();

	/*Optimazing right eye*/
	SelectOptRange(331, 480, 601, 770);
	OptimizingDepth();

	/*Optimazing rightface*/
	SelectOptRange(481, 600, 681, 760);
	OptimizingDepth();

	/*Optimazing leftface*/
	SelectOptRange(481, 600, 420, 480);
	OptimizingDepth();

	///*Optimazing forehead*/
	//SelectOptRange(200, 360, 400, 770);
	//OptimizingDepth();

	//InportOPTDepth_16UC1(depthImage);

	InportOPTDepth_32FC1(depthImage);

	printf("Shape from shading end!\n");
}

void DepthSFS::SelectingFaceRange()
{
	bool** currentMark = new bool*[mHeight];
	for (int i = 0; i < mHeight; i++)
	{
		currentMark[i] = new bool[mWidth];
		for (int j = 0; j < mWidth; j++)
			currentMark[i][j] = false;
	}

	for (MyMesh::FPtr pF : It::MFIterator(mMesh))
	{
		double dU = 0;
		double dV = 0;

		for (MyMesh::VPtr pV : It::FVIterator(pF))
		{
			dU += pV->uv()[0] * mWidth;
			dV += pV->uv()[1] * mHeight;
		}
		int iU = dU / 3;
		int iV = mHeight - dV / 3;

		if (iU > 0 && iU < (mWidth - 1) && iV > 0 && iV < (mHeight - 1))
			currentMark[iV][iU] = true;
	}

	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (currentMark[i][j]) {
				int offsetNum = 5;
				int offset[4] = { 0 };
				for (int k = 1; k <= offsetNum; k++)
				{
					if ((i - k) > 0)
						if (currentMark[i - k][j])
							offset[0] = k;
					if ((i + k) < mHeight - 1)
						if (currentMark[i + k][j])
							offset[1] = k;
					if ((j - k) > 0)
						if (currentMark[i][j - k])
							offset[2] = k;
					if ((j + k) < mWidth - 1)
						if (currentMark[i][j + k])
							offset[3] = k;
				}
				for (int k = 1; k < offset[0]; k++)
					currentMark[i - k][j] = true;
				for (int k = 1; k < offset[1]; k++)
					currentMark[i + k][j] = true;
				for (int k = 1; k < offset[2]; k++)
					currentMark[i][j - k] = true;
				for (int k = 1; k < offset[3]; k++)
					currentMark[i][j + k] = true;
			}
		}
	}
	/*Filtrate some illegal points*/
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (currentMark[i][j])
			{
				if (currentMark[i + 1][j] && currentMark[i - 1][j] && currentMark[i][j + 1] && currentMark[i][j - 1])
				{
					mFaceMark[i][j] = true;
					mFaceMark[i - 1][j] = true;
					mFaceMark[i + 1][j] = true;
					mFaceMark[i][j - 1] = true;
					mFaceMark[i][j + 1] = true;
				}
			}
		}
	}
	mFaceSize = 0;
	for (int i = 0; i < mHeight; i++)
		for (int j = 0; j < mWidth; j++)
			if (mFaceMark[i][j])
				mFaceSize++;
	/*Init depth*/
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (!mFaceMark[i][j])
				mDepths[i][j] = 0;
			else
				mDepths[i][j] *= 10;
		}
	}
	/*Smooth depth*/
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mDepths[i][j] != 0) {
				int offsetNum = 5;
				int offset[4] = { 0 };
				for (int k = 1; k <= offsetNum; k++)
				{
					if ((i - k) > 0)
						if (mDepths[i - k][j] != 0)
							offset[0] = k;
					if ((i + k) < mHeight - 1)
						if (mDepths[i + k][j] != 0)
							offset[1] = k;
					if ((j - k) > 0)
						if (mDepths[i][j - k] != 0)
							offset[2] = k;
					if ((j + k) < mWidth - 1)
						if (mDepths[i][j + k] != 0)
							offset[3] = k;
				}
				for (int k = 1; k < offset[0]; k++)
				{
					int maxOffset = offset[0];
					double depthDiff = ((mDepths[i - maxOffset][j] - mDepths[i][j]) * k / maxOffset);
					mDepths[i - k][j] = mDepths[i][j] + depthDiff;
				}
				for (int k = 1; k < offset[1]; k++)
				{
					int maxOffset = offset[1];
					double depthDiff = ((mDepths[i + maxOffset][j] - mDepths[i][j]) * k / maxOffset);
					mDepths[i + k][j] = mDepths[i][j] + depthDiff;
				}
				for (int k = 1; k < offset[2]; k++)
				{
					int maxOffset = offset[2];
					double depthDiff = ((mDepths[i][j - maxOffset] - mDepths[i][j]) * k / maxOffset);
					mDepths[i][j - k] = mDepths[i][j] + depthDiff;
				}
				for (int k = 1; k < offset[3]; k++)
				{
					int maxOffset = offset[3];
					double depthDiff = ((mDepths[i][j + maxOffset] - mDepths[i][j]) * k / maxOffset);
					mDepths[i][j + k] = mDepths[i][j] + depthDiff;
				}
			}
		}
	}
	/*Init pre depth*/
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			mPreDepths[i][j] = mDepths[i][j];
		}
	}
	/*Free temporary memory*/
	for (int i = 0; i < mHeight; i++)
		delete[] currentMark[i];
	delete[] currentMark;
}

void DepthSFS::ComputeNormal()
{
	bool** currentMark = new bool*[mHeight];
	for (int i = 0; i < mHeight; i++)
	{
		currentMark[i] = new bool[mWidth];
		for (int j = 0; j < mWidth; j++)
			currentMark[i][j] = false;
	}
	/*Compute mesh normal by using depth information*/
	for (int i = 1; i < mHeight; i++)
	{
		for (int j = 1; j < mWidth; j++)
		{
			if (mFaceMark[i][j] && mFaceMark[i - 1][j] && mFaceMark[i][j - 1])
			{
				int uv_U = j;
				int uv_V = mHeight - 1 - i;

				double Cx = mCameraParam.cameraCx, Cy = mCameraParam.cameraCy,
					Fx = mCameraParam.cameraFx, Fy = mCameraParam.cameraFy;

				double Dij = mDepths[i][j], Dcj = mDepths[i - 1][j], Did = mDepths[i][j - 1];

				double nX = Did * (Dij - Dcj) / Fy;
				double nY = Dcj * (Dij - Did) / Fx;
				double nZ = nX * (Cx - uv_U) / Fx + nY * (Cy - uv_V) / Fy - Dcj * Did / (Fx * Fy);

				double nNorm = sqrt(nX * nX + nY * nY + nZ * nZ);

				if (nNorm != 0)
				{
					mDepthNormal[i][j].nx = nX / nNorm;
					mDepthNormal[i][j].ny = nY / nNorm;
					mDepthNormal[i][j].nz = nZ / nNorm;
				}
				currentMark[i][j] = true;
			}
		}
	}
	/*Mark normal range*/
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (currentMark[i][j])
			{
				if (currentMark[i + 1][j] && currentMark[i - 1][j] && currentMark[i][j + 1] && currentMark[i][j - 1])
				{
					mNormalMark[i][j] = true;
					mNormalMark[i - 1][j] = true;
					mNormalMark[i + 1][j] = true;
					mNormalMark[i][j - 1] = true;
					mNormalMark[i][j + 1] = true;
				}
			}
		}
	}
	
	mNormalSize = 0;
	for (int i = 0; i < mHeight; i++)
		for (int j = 0; j < mWidth; j++)
			if (mNormalMark[i][j])
				mNormalSize++;
	/*Free temporary memory*/
	for (int i = 0; i < mHeight; i++)
		delete[] currentMark[i];
	delete[] currentMark;
}

void DepthSFS::ComputeAlbedos()
{
	/*Init illumination coefficient*/
	double ** matA = new double*[mNormalSize];
	for (int i = 0; i < mNormalSize; i++)
		matA[i] = new double[9];

	int nowIndex = 0;
	/*Compute spherical harmonic from face pixels*/
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mNormalMark[i][j])
			{
				double nX = mDepthNormal[i][j].nx;
				double nY = mDepthNormal[i][j].ny;
				double nZ = mDepthNormal[i][j].nz;
				matA[nowIndex][0] = 1.0f;
				matA[nowIndex][1] = nY;
				matA[nowIndex][2] = nZ;
				matA[nowIndex][3] = nX;
				matA[nowIndex][4] = nX * nY;
				matA[nowIndex][5] = nY * nZ;
				matA[nowIndex][6] = 2 * nZ * nZ - nX * nX - nY * nY;
				matA[nowIndex][7] = nZ * nX;
				matA[nowIndex][8] = nX * nX - nY * nY;
				nowIndex++;
			}
		}
	}

	Matrix<double> matB(9, 9, 0);
	for (int i = 0; i < nowIndex; i++)
		for (int p = 0; p < 9; p++)
			for (int q = 0; q < 9; q++)
				matB(p, q) += matA[i][p] * matA[i][q];

	Matrix<double> matB_INV = matB.inverse();

	vector<double> AT_I;
	AT_I.reserve(9);

	nowIndex = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mNormalMark[i][j])
			{
				for (int k = 0; k < 9; k++)
					AT_I[k] += matA[nowIndex][k] * mGrayIntensity[i][j];
				nowIndex++;
			}
		}
	}

	LightIllumination.reserve(9);

	for (int i = 0; i < 9; i++)
		for (int j = 0; j < 9; j++)
			LightIllumination[i] += matB_INV(i, j) * AT_I[j];

	nowIndex = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mNormalMark[i][j])
			{
				double coefficient = 0;
				for (int k = 0; k < 9; k++)
					coefficient += LightIllumination[k] * matA[nowIndex][k];

				mAlbedos[i][j][0] = (double)mRGBIntensity[i][j].r / coefficient;
				mAlbedos[i][j][1] = (double)mRGBIntensity[i][j].g / coefficient;
				mAlbedos[i][j][2] = (double)mRGBIntensity[i][j].b / coefficient;

				nowIndex++;
			}
		}
	}
	/*Free temporary memory*/
	for (int i = 0; i < mNormalSize; i++)
		delete[] matA[i];
	delete[] matA;
}

void DepthSFS::SelectOptRange(int startRow, int endRow, int startCol, int endCol)
{
	/*Init Optimazing range*/
	for (int i = 0; i < mHeight; i++)
		for (int j = 0; j < mWidth; j++)
			mOPTRangeMark[i][j] = false;

	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mNormalMark[i][j] && i >= startRow && i < endRow && j > startCol && j < endCol)
			{
				if (mNormalMark[i][j] && mNormalMark[i - 1][j] && mNormalMark[i][j - 1]
					&& mNormalMark[i + 1][j] && mNormalMark[i][j + 1]
					&& mNormalMark[i + 1][j - 1] && mNormalMark[i - 1][j + 1])
				{
					mOPTRangeMark[i][j] = true;
					mOPTRangeMark[i - 1][j] = true;
					mOPTRangeMark[i][j - 1] = true;
					mOPTRangeMark[i + 1][j] = true;
					mOPTRangeMark[i][j + 1] = true;
					mOPTRangeMark[i + 1][j - 1] = true;
					mOPTRangeMark[i - 1][j + 1] = true;
				}
			}
		}
	}

	mOPTRangeSize = 0;
	for (int i = 0; i < mHeight; i++)
		for (int j = 0; j < mWidth; j++)
			if (mOPTRangeMark[i][j])
				mOPTRangeSize++;
}

void DepthSFS::OptimizingDepth()
{
	double opts[SPLM_OPTS_SZ], info[SPLM_INFO_SZ];
	opts[0] = SPLM_INIT_MU;
	opts[1] = SPLM_STOP_THRESH;
	opts[2] = SPLM_STOP_THRESH;
	opts[3] = SPLM_STOP_THRESH;
	opts[4] = SPLM_DIFF_DELTA;
	opts[5] = SPLM_CHOLMOD;

	int paramNum = mOPTRangeSize;
	int measureNum = 0;
	int nnz = 0;
	int ret = -1;
	
	printf("Optimazing range size: %d\n", mOPTRangeSize);

	double* params = new double[mOPTRangeSize];

	for (int i = 0; i < 9; i++)
		paramLI_OPT.push_back(LightIllumination[i]);
	
	connectedIndex_OPT = new int*[mOPTRangeSize];
	for (int i = 0; i < mOPTRangeSize; i++)
		connectedIndex_OPT[i] = new int[6];

	paramIij_OPT = new CPoint2[mOPTRangeSize];

	paramUV_OPT = new Coordinate_UV[mOPTRangeSize];

	normalPairs_OPT = new NormalPairs*[mOPTRangeSize];
	for (int i = 0; i < mOPTRangeSize; i++)
		normalPairs_OPT[i] = new NormalPairs[4];

	int** connectMap = new int*[mHeight];
	for (int i = 0; i < mHeight; i++)
	{
		connectMap[i] = new int[mWidth];
		for (int j = 0; j < mWidth; j++)
			connectMap[i][j] = -1;
	}

	int nowIndex = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mOPTRangeMark[i][j])
			{
				params[nowIndex] = mDepths[i][j];

				preDepth_OPT.push_back(mPreDepths[i][j]);

				connectMap[i][j] = nowIndex;

				nowIndex++;
			}
		}
	}

	nowIndex = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mOPTRangeMark[i][j])
			{
				paramIij_OPT[nowIndex][0] = (mGrayIntensity[i][j] - mGrayIntensity[i + 1][j]) * 2;
				paramIij_OPT[nowIndex][1] = (mGrayIntensity[i][j] - mGrayIntensity[i][j + 1]) * 2;

				paramUV_OPT[nowIndex].uv_U = j;
				paramUV_OPT[nowIndex].uv_V = mHeight - 1 - i;

				connectedIndex_OPT[nowIndex][0] = connectMap[i - 1][j];
				connectedIndex_OPT[nowIndex][1] = connectMap[i][j - 1];
				connectedIndex_OPT[nowIndex][2] = connectMap[i + 1][j];
				connectedIndex_OPT[nowIndex][3] = connectMap[i][j + 1];
				connectedIndex_OPT[nowIndex][4] = connectMap[i + 1][j - 1];
				connectedIndex_OPT[nowIndex][5] = connectMap[i - 1][j + 1];

				if (connectedIndex_OPT[nowIndex][0] != -1 && connectedIndex_OPT[nowIndex][1] != -1
					&& connectedIndex_OPT[nowIndex][2] != -1 && connectedIndex_OPT[nowIndex][3] != -1
					&& connectedIndex_OPT[nowIndex][4] != -1 && connectedIndex_OPT[nowIndex][5] != -1)
					ParamIndexs_OPT.push_back(nowIndex);

				nowIndex++;
			}
		}
	}

	nowIndex = 0;
	int ParamIndexs_OPT_Index = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mOPTRangeMark[i][j])
			{
				if (nowIndex == ParamIndexs_OPT[ParamIndexs_OPT_Index])
				{
					/*f(dij, dcj, did, daj, dad) = ((Bij - Baj) - (Iij - Iaj))*/
					measureNum++;
					nnz += 5;
					/*f(dij, dcj, did, dib, dcb) = ((Bij - Bib) - (Iij - Iib))*/
					measureNum++;
					nnz += 5;
					/*f(dij, daj, dcj, dib, did) = (Pij.x - 0.25*(Paj.x + Pib.x + Pcj.x + Pid.x)*/
					measureNum++;
					nnz += 5;
					/*f(dij, daj, dcj, dib, did) = (Pij.y - 0.25*(Paj.y + Pib.y + Pcj.y + Pid.y)*/
					measureNum++;
					nnz += 5;
					/*f(dij, daj, dcj, dib, did) = (Pij.z - 0.25*(Paj.z + Pib.z + Pcj.z + Pid.z)*/
					measureNum++;
					nnz += 5;
					/*f(dij) = (dij - predij)*/
					measureNum++;
					nnz += 1;
					ParamIndexs_OPT_Index++;
				}
				nowIndex++;
			}
		}
	}

	CP_OPT.cameraCx = mCameraParam.cameraCx;
	CP_OPT.cameraCy = mCameraParam.cameraCy;
	CP_OPT.cameraFx = mCameraParam.cameraFx;
	CP_OPT.cameraFy = mCameraParam.cameraFy;

	Height_OPT = mHeight;
	Width_OPT = mWidth;

	nowIndex = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mOPTRangeMark[i][j])
			{
				double Dij = mDepths[i][j], Dcj = mDepths[i - 1][j], Did = mDepths[i][j - 1];
				/*f(dij, dcj, did, daj, dad) = ((Bij - Baj) - (Iij - Iaj))*/
				ConnectDepth Bij_CD;
				Bij_CD.D = Dij;
				Bij_CD.D_top = Dcj;
				Bij_CD.D_left = Did;

				NormalProp _unitNormal;
				NormalProp _initNormal;

				Coordinate_UV currentUV;
				currentUV.uv_U = j;
				currentUV.uv_V = mHeight - 1 - i;

				double Bij = ComputeShading(currentUV, Bij_CD,
					_unitNormal, _initNormal);

				mShading[i][j] = Bij;

				nowIndex++;
			}
		}
	}

	for (int i = 0; i < mHeight; i++)
		for (int j = 0; j < mWidth; j++)
			if (mOPTRangeMark[i][j])
				resultShowGray.ptr<uchar>(i)[j] = mShading[i][j];
			else
				resultShowGray.ptr<uchar>(i)[j] = 0;
	cv::imshow("resultShowGray", resultShowGray);
	cv::waitKey();

	printf("Start optimazing......!\n");
	//ret = sparselm_dercrs(OptimizingDepthFunction, OptimizingDepthAnjacCRS, params, NULL, paramNum, 0, measureNum, nnz, -1, 1000, opts, info, NULL);
	ret = sparselm_difcrs(OptimizingDepthFunction, OptimizingDepthZPJacCRS, params, NULL, paramNum, 0, measureNum, nnz, -1, 1000, opts, info, NULL);
	printf("Iterations:\t%d\n", ret);

	double totalError = 0;
	
	nowIndex = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mOPTRangeMark[i][j])
			{
				totalError += abs(params[nowIndex] - mDepths[i][j]);

				mDepths[i][j] = params[nowIndex];

				nowIndex++;
			}
		}
	}
	printf("TotalError:\t%lf\n\n", totalError);

	nowIndex = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mOPTRangeMark[i][j])
			{
				double Dij = mDepths[i][j], Dcj = mDepths[i - 1][j], Did = mDepths[i][j - 1];
				/*f(dij, dcj, did, daj, dad) = ((Bij - Baj) - (Iij - Iaj))*/
				ConnectDepth Bij_CD;
				Bij_CD.D = Dij;
				Bij_CD.D_top = Dcj;
				Bij_CD.D_left = Did;

				NormalProp _unitNormal;
				NormalProp _initNormal;

				Coordinate_UV currentUV;
				currentUV.uv_U = j;
				currentUV.uv_V = mHeight - 1 - i;

				double Bij = ComputeShading(currentUV, Bij_CD,
					_unitNormal, _initNormal);

				mShading[i][j] = Bij;

				nowIndex++;
			}
		}
	}

	for (int i = 0; i < mHeight; i++)
		for (int j = 0; j < mWidth; j++)
			if (mOPTRangeMark[i][j])
				resultShowGray.ptr<uchar>(i)[j] = mShading[i][j];
			else
				resultShowGray.ptr<uchar>(i)[j] = 0;
	cv::imshow("resultShowGray", resultShowGray);
	cv::waitKey();

	/*Free memory*/
	for (int i = 0; i < mHeight; i++)
		delete[] connectMap[i];
	for (int i = 0; i < mOPTRangeSize; i++)
	{
		delete[] connectedIndex_OPT[i];
		delete[] normalPairs_OPT[i];
	}
	delete[] params;
	delete[] paramIij_OPT;
	delete[] paramUV_OPT;
	delete[] connectedIndex_OPT;
	delete[] normalPairs_OPT;
	delete[] connectMap;

	params = NULL;
	paramIij_OPT = NULL;
	paramUV_OPT = NULL;
	connectedIndex_OPT = NULL;
	normalPairs_OPT = NULL;
	connectMap = NULL;

	/*Reset*/
	Height_OPT = 0;
	Width_OPT = 0;

	CP_OPT.cameraCx = 0;
	CP_OPT.cameraCy = 0;
	CP_OPT.cameraFx = 0;
	CP_OPT.cameraFy = 0;

	ParamIndexs_OPT.clear();
	paramLI_OPT.clear();
	preDepth_OPT.clear();
}

void DepthSFS::InportOPTDepth_16UC1(cv::Mat& depthImage)
{
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceMark[i][j])
			{
				ushort currentDepth = round(mDepths[i][j]);
				depthImage.ptr<ushort>(i)[j] = currentDepth;
			}
			else
			{
				depthImage.ptr<ushort>(i)[j] = 0;
			}
		}
	}
}

void DepthSFS::InportOPTDepth_32FC1(cv::Mat& depthImage)
{
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceMark[i][j])
			{
				float currentDepth = mDepths[i][j];
				depthImage.ptr<float>(i)[j] = currentDepth;
			}
			else
			{
				depthImage.ptr<float>(i)[j] = 0;
			}
		}
	}
}