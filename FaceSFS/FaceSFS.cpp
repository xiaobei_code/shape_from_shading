#include "FaceSFS.h"

#define DIV(n, m)     ((n) / (m))
#define NORM_PQ(mP, mQ)		(sqrt((mP*mP) + (mQ*mQ) + 1))

/*
* Optimazing the Function(harmonic coefficient)
*/
VectorSH* mVectorSH_OPT_HE;
double* mParamRij_OPT_HE;
double* mParamIij_OPT_HE;

void OptimizingHECost(double *p, double *hx, int m, int n, void *data)
{
	register int k, currentIndex;

	for (k = 0; k < n; k++)
	{
		currentIndex = k;

		double rij = mParamRij_OPT_HE[currentIndex];
		double Iij = mParamIij_OPT_HE[currentIndex];

		double h1 = mVectorSH_OPT_HE[currentIndex].mH[0], h2 = mVectorSH_OPT_HE[currentIndex].mH[1],
			h3 = mVectorSH_OPT_HE[currentIndex].mH[2], h4 = mVectorSH_OPT_HE[currentIndex].mH[3],
			h5 = mVectorSH_OPT_HE[currentIndex].mH[4], h6 = mVectorSH_OPT_HE[currentIndex].mH[5],
			h7 = mVectorSH_OPT_HE[currentIndex].mH[6], h8 = mVectorSH_OPT_HE[currentIndex].mH[7],
			h9 = mVectorSH_OPT_HE[currentIndex].mH[8];

		hx[k] = rij * (p[0] * h1 + p[1] * h2 + p[2] * h3 + p[3] * h4 + p[4] * h5 + p[5] * h6 + p[6] * h7 + p[7] * h8 + p[8] * h9) - Iij;
	}
}

void OptimizingHECostJac(double *p, double *jac, int m, int n, void *data)
{
	register int k, currentIndex, currentJacIndex;

	for (k = currentJacIndex = 0; k < n; k++)
	{
		currentIndex = k;

		double rij = mParamRij_OPT_HE[currentIndex];

		double h1 = mVectorSH_OPT_HE[currentIndex].mH[0], h2 = mVectorSH_OPT_HE[currentIndex].mH[1],
			h3 = mVectorSH_OPT_HE[currentIndex].mH[2], h4 = mVectorSH_OPT_HE[currentIndex].mH[3],
			h5 = mVectorSH_OPT_HE[currentIndex].mH[4], h6 = mVectorSH_OPT_HE[currentIndex].mH[5],
			h7 = mVectorSH_OPT_HE[currentIndex].mH[6], h8 = mVectorSH_OPT_HE[currentIndex].mH[7],
			h9 = mVectorSH_OPT_HE[currentIndex].mH[8];

		jac[currentJacIndex] = rij * h1;
		currentJacIndex++;
		jac[currentJacIndex] = rij * h2;
		currentJacIndex++;
		jac[currentJacIndex] = rij * h3;
		currentJacIndex++;
		jac[currentJacIndex] = rij * h4;
		currentJacIndex++;
		jac[currentJacIndex] = rij * h5;
		currentJacIndex++;
		jac[currentJacIndex] = rij * h6;
		currentJacIndex++;
		jac[currentJacIndex] = rij * h7;
		currentJacIndex++;
		jac[currentJacIndex] = rij * h8;
		currentJacIndex++;
		jac[currentJacIndex] = rij * h9;
		currentJacIndex++;
	}
}

/*
* Optimazing the Function(dij)
*/
double mHE_OPT_PCV[9];
int** mConnectIndex_OPT_PCV;
vector<int> ParamIndexs_OPT_PCV;

void OptimizingPCVCost(double *p, double *hx, int m, int n, void *data)
{
	register int k, currentIndex, upIndex, bottomIndex, leftIndex, rightIndex, kPlus1;

	for (k = 0; k < n; k++) {
		kPlus1 = k + 1;
		int ParamIndexs_OPT_PCV_Index = DIV(k, 5);
		currentIndex = ParamIndexs_OPT_PCV[ParamIndexs_OPT_PCV_Index];

		upIndex = mConnectIndex_OPT_PCV[currentIndex][0];
		bottomIndex = mConnectIndex_OPT_PCV[currentIndex][1];
		leftIndex = mConnectIndex_OPT_PCV[currentIndex][2];
		rightIndex = mConnectIndex_OPT_PCV[currentIndex][3];

		double dij = p[currentIndex], daj = p[bottomIndex], dcj = p[upIndex],
			dib = p[rightIndex], did = p[leftIndex];

		if (kPlus1 % 5 == 1) //f(dij) = rij * (HE * EH + dij) - Iij
		{
			double rij = mParamRij_OPT_HE[currentIndex];
			double Iij = mParamIij_OPT_HE[currentIndex];

			double h1 = mVectorSH_OPT_HE[currentIndex].mH[0], h2 = mVectorSH_OPT_HE[currentIndex].mH[1],
				h3 = mVectorSH_OPT_HE[currentIndex].mH[2], h4 = mVectorSH_OPT_HE[currentIndex].mH[3],
				h5 = mVectorSH_OPT_HE[currentIndex].mH[4], h6 = mVectorSH_OPT_HE[currentIndex].mH[5],
				h7 = mVectorSH_OPT_HE[currentIndex].mH[6], h8 = mVectorSH_OPT_HE[currentIndex].mH[7],
				h9 = mVectorSH_OPT_HE[currentIndex].mH[8];

			double e1 = mHE_OPT_PCV[0], e2 = mHE_OPT_PCV[1], e3 = mHE_OPT_PCV[2],
				e4 = mHE_OPT_PCV[3], e5 = mHE_OPT_PCV[4], e6 = mHE_OPT_PCV[5],
				e7 = mHE_OPT_PCV[6], e8 = mHE_OPT_PCV[7], e9 = mHE_OPT_PCV[8];
			hx[k] = (rij * ((e1*h1 + e2 * h2 + e3 * h3 + e4 * h4
				+ e5 * h5 + e6 * h6 + e7 * h7 + e8 * h8 + e9 * h9) + dij) - Iij);
		}
		else if (kPlus1 % 5 == 2) //f(dij) = dij
			hx[k] = dij;
		else if (kPlus1 % 5 == 3) //f(daj, dij) = daj - dij
			hx[k] = daj - dij;
		else if (kPlus1 % 5 == 4) //f(dib, dij) = dib - dij
			hx[k] = dib - dij;
		else if (kPlus1 % 5 == 0) //f(dij, daj, dib, dcj, did) = 4*dij - (daj + dib + dcj + did)
			hx[k] = 4 * dij - (daj + dib + dcj + did);
	}
}

void OptimizingPCVCostAnjacCRS(double *p, struct splm_crsm *jac, int m, int n, void *adata)
{
	register int currentIndex, bottomIndex, rightIndex, upIndex, leftIndex, kPlus1, k;
	register int currentJacIndex = 0;

	for (k = 0; k < n; k++) {
		jac->rowptr[k] = currentJacIndex;

		kPlus1 = k + 1;
		int ParamIndexs_OPT_PCV_Index = DIV(k, 5);
		currentIndex = ParamIndexs_OPT_PCV[ParamIndexs_OPT_PCV_Index];

		upIndex = mConnectIndex_OPT_PCV[currentIndex][0];
		bottomIndex = mConnectIndex_OPT_PCV[currentIndex][1];
		leftIndex = mConnectIndex_OPT_PCV[currentIndex][2];
		rightIndex = mConnectIndex_OPT_PCV[currentIndex][3];

		if (kPlus1 % 5 == 1) //Diff(rij * (HE * EH + dij) - Iij)
		{
			double rij = mParamRij_OPT_HE[currentIndex];
			/*Diff(f(dij), dij)*/
			jac->val[currentJacIndex] = rij;
			jac->colidx[currentJacIndex] = currentIndex;
			currentJacIndex++;
		}
		else if (kPlus1 % 5 == 2) //Diff(dij)
		{
			/*Diff(f(dij), dij)*/
			jac->val[currentJacIndex] = 1;
			jac->colidx[currentJacIndex] = currentIndex;
			currentJacIndex++;
		}
		else if (kPlus1 % 5 == 3) //Diff(daj - dij)
		{
			/*Diff(f(daj, dij), dij)*/
			jac->val[currentJacIndex] = -1;
			jac->colidx[currentJacIndex] = currentIndex;
			currentJacIndex++;
			/*Diff(f(daj, dij), daj)*/
			jac->val[currentJacIndex] = 1;
			jac->colidx[currentJacIndex] = bottomIndex;
			currentJacIndex++;
		}
		else if (kPlus1 % 5 == 4) //Diff(dib - dij)
		{
			/*Diff(f(daj, dij), dij)*/
			jac->val[currentJacIndex] = -1;
			jac->colidx[currentJacIndex] = currentIndex;
			currentJacIndex++;
			/*Diff(f(dib, dij), dib)*/
			jac->val[currentJacIndex] = 1;
			jac->colidx[currentJacIndex] = rightIndex;
			currentJacIndex++;
		}
		else if (kPlus1 % 5 == 0) //Diff(4*dij - (daj + dib + dcj + did))
		{
			/*Diff(f(daj, dij), dij)*/
			jac->val[currentJacIndex] = 4;
			jac->colidx[currentJacIndex] = currentIndex;
			currentJacIndex++;
			/*Diff(f(dcj, dij), dcj)*/
			jac->val[currentJacIndex] = -1;
			jac->colidx[currentJacIndex] = upIndex;
			currentJacIndex++;
			/*Diff(f(daj, dij), daj)*/
			jac->val[currentJacIndex] = -1;
			jac->colidx[currentJacIndex] = bottomIndex;
			currentJacIndex++;
			/*Diff(f(did, dij), did)*/
			jac->val[currentJacIndex] = -1;
			jac->colidx[currentJacIndex] = leftIndex;
			currentJacIndex++;
			/*Diff(f(dib, dij), dib)*/
			jac->val[currentJacIndex] = -1;
			jac->colidx[currentJacIndex] = rightIndex;
			currentJacIndex++;
		}
	}
	jac->rowptr[n] = currentJacIndex;
}

/*
* Optimazing the Egrad && Eclosed && Esmooth && Eint
*/
vector<int> ParamIndexs_OPT_ENERGY;
vector<bool> ifZero_OPT_ENERGY; //True->0 false->1
CPoint* paramRij_OPT_ENERGY;
CPoint* paramNormij_OPT_ENERGY;
CPoint2* paramIij_OPT_ENERGY;
int** connectIndex_OPT_ENERGY;
VectorHE functionHE_OPT_ENERGY;

double ComputeFunctionS(const double& currentR, const double& currentP, const double& currentQ)
{
	double nx = currentP;
	double ny = currentQ;
	double nz = -1;
	double sqrtValue = NORM_PQ(nx, ny);
	nx /= sqrtValue;
	ny /= sqrtValue;
	nz /= sqrtValue;
	double returnValue = functionHE_OPT_ENERGY.mE[0] + functionHE_OPT_ENERGY.mE[1] * nx + functionHE_OPT_ENERGY.mE[2] * ny
		+ functionHE_OPT_ENERGY.mE[3] * nz + functionHE_OPT_ENERGY.mE[4] * nx * ny + functionHE_OPT_ENERGY.mE[5] * nx * nz
		+ functionHE_OPT_ENERGY.mE[6] * ny * nz + functionHE_OPT_ENERGY.mE[7] * (nx * nx - ny * ny)
		+ functionHE_OPT_ENERGY.mE[8] * (3 * nz * nz - 1);
	return (currentR * returnValue);
}

double ComputeFunctionEgradDiffPart_P(const double& pij, const double& qij)
{
	double mNorm = NORM_PQ(pij, qij);
	double mNorm3 = mNorm * mNorm * mNorm;

	return ((functionHE_OPT_ENERGY.mE[1] - functionHE_OPT_ENERGY.mE[5] + 2 * functionHE_OPT_ENERGY.mE[7] * pij - 2 * functionHE_OPT_ENERGY.mE[8] * pij
		+ functionHE_OPT_ENERGY.mE[4] * qij) / mNorm + (functionHE_OPT_ENERGY.mE[3] * pij - functionHE_OPT_ENERGY.mE[1] * pij*pij
			+ functionHE_OPT_ENERGY.mE[5] * pij*pij - functionHE_OPT_ENERGY.mE[2] * pij*qij + functionHE_OPT_ENERGY.mE[6] * pij*qij
			- functionHE_OPT_ENERGY.mE[4] * pij*pij*qij + functionHE_OPT_ENERGY.mE[8] * pij*(pij*pij + qij * qij* -2)
			- functionHE_OPT_ENERGY.mE[7] * pij*(pij*pij - qij * qij)) / mNorm3);
}

double ComputeFunctionEgradDiffPart_Q(const double& pij, const double& qij)
{
	double mNorm = NORM_PQ(pij, qij);
	double mNorm3 = mNorm * mNorm * mNorm;

	return ((functionHE_OPT_ENERGY.mE[6] - functionHE_OPT_ENERGY.mE[2] - functionHE_OPT_ENERGY.mE[4] * pij + 2 * functionHE_OPT_ENERGY.mE[7] * qij
		+ 2 * functionHE_OPT_ENERGY.mE[8] * qij) / mNorm + (-1 * functionHE_OPT_ENERGY.mE[3] * qij + functionHE_OPT_ENERGY.mE[2] * qij * qij
			- functionHE_OPT_ENERGY.mE[6] * qij * qij + functionHE_OPT_ENERGY.mE[1] * pij * qij - functionHE_OPT_ENERGY.mE[5] * pij * qij
			+ functionHE_OPT_ENERGY.mE[4] * pij * qij * qij - functionHE_OPT_ENERGY.mE[8] * qij * (pij * pij + qij * qij - 2)
			+ functionHE_OPT_ENERGY.mE[7] * qij * (pij * pij - qij * qij)) / mNorm3);
}


void OptimizingCost(double *p, double *hx, int m, int n, void *adata)
{
	register int currentIndex, pIndex, qIndex, pBottomIndex, qBottomIndex,
		pRightIndex, qRightIndex, k, kPlus1;

	for (k = 0; k < n; k++) {
		kPlus1 = k + 1;
		int ParamIndexs_OPT_ENERGY_Index = DIV(k, 11);
		currentIndex = ParamIndexs_OPT_ENERGY[ParamIndexs_OPT_ENERGY_Index];

		pIndex = 2 * currentIndex;
		qIndex = pIndex + 1;
		pBottomIndex = connectIndex_OPT_ENERGY[currentIndex][0];
		qBottomIndex = pBottomIndex + 1;
		pRightIndex = connectIndex_OPT_ENERGY[currentIndex][1];
		qRightIndex = pRightIndex + 1;

		double Fij = 0, Faj = 0, Fib = 0;

		double Pij = p[pIndex], Qij = p[qIndex];
		double Paj = p[pBottomIndex], Qaj = p[qBottomIndex];
		double Pib = p[pRightIndex], Qib = p[qRightIndex];

		if (kPlus1 % 11 == 1) //f(pij, qij, paj, qaj) = (Saj - Sij)
		{
			double rij = paramRij_OPT_ENERGY[currentIndex][0];
			double raj = paramRij_OPT_ENERGY[currentIndex][1];
			double Iaj = paramIij_OPT_ENERGY[currentIndex][0];
			if (!ifZero_OPT_ENERGY[pIndex])
				Fij = ComputeFunctionS(rij, Pij, Qij);
			if (!ifZero_OPT_ENERGY[pBottomIndex])
				Faj = ComputeFunctionS(raj, p[pBottomIndex], p[qBottomIndex]);
			hx[k] = (Faj - Fij - Iaj);
		}
		else if (kPlus1 % 11 == 2) //f(pij, qij, pib, qib) = (Sib - Sij)
		{
			double rij = paramRij_OPT_ENERGY[currentIndex][0];
			double rib = paramRij_OPT_ENERGY[currentIndex][2];
			double Iib = paramIij_OPT_ENERGY[currentIndex][1];
			if (!ifZero_OPT_ENERGY[pIndex])
				Fij = ComputeFunctionS(rij, Pij, Qij);
			if (!ifZero_OPT_ENERGY[pRightIndex])
				Fib = ComputeFunctionS(rib, p[pRightIndex], p[qRightIndex]);
			hx[k] = (Fib - Fij - Iib);
		}
		else if (kPlus1 % 11 == 3) //f(pij, qij) = pij / sqrt(pij^2 + qij^2 + 1)
		{
			double Nx = paramNormij_OPT_ENERGY[currentIndex][0];
			hx[k] = (Pij / NORM_PQ(Pij, Qij) - Nx);
		}
		else if (kPlus1 % 11 == 4) //f(pij, qij) = qij / sqrt(pij^2 + qij^2 + 1)
		{
			double Ny = paramNormij_OPT_ENERGY[currentIndex][1];
			hx[k] = (Qij / NORM_PQ(Pij, Qij) - Ny);
		}
		else if (kPlus1 % 11 == 5) //f(pij, qij) = (-1) / sqrt(pij^2 + qij^2 + 1)
		{
			double Nz = paramNormij_OPT_ENERGY[currentIndex][2];
			hx[k] = ((-1) / NORM_PQ(Pij, Qij) - Nz);
		}
		/*f(pij, qij, paj, qaj) = (paj / sqrt(paj^2 + qaj^2 + 1) - pij / sqrt(pij^2 + qij^2 +1))*/
		else if (kPlus1 % 11 == 6)
		{
			Fij = Pij / NORM_PQ(Pij, Qij);
			Faj = Paj / NORM_PQ(Paj, Qaj);
			hx[k] = (Faj - Fij);
		}
		/*f(pij, qij, paj, qaj) = (qaj / sqrt(paj^2 + qaj^2 + 1) - qij / sqrt(pij^2 + qij^2 +1))*/
		else if (kPlus1 % 11 == 7)
		{
			Fij = Qij / NORM_PQ(Pij, Qij);
			Faj = Qaj / NORM_PQ(Paj, Qaj);
			hx[k] = (Faj - Fij);
		}
		/*f(pij, qij, paj, qaj) = (-1 / sqrt(paj^2 + qaj^2 + 1) + 1 / sqrt(pij^2 + qij^2 +1))*/
		else if (kPlus1 % 11 == 8)
		{
			Fij = -1 / NORM_PQ(Pij, Qij);
			Faj = -1 / NORM_PQ(Paj, Qaj);
			hx[k] = (Faj - Fij);
		}
		/*f(pij, qij, pib, qib) = (pib / sqrt(pib^2 + qib^2 + 1) - pij / sqrt(pij^2 + qij^2 +1))*/
		else if (kPlus1 % 11 == 9)
		{
			Fij = Pij / NORM_PQ(Pij, Qij);
			Fib = Pib / NORM_PQ(Pib, Qib);
			hx[k] = (Fib - Fij);
		}
		/*f(pij, qij, pib, qib) = (qib / sqrt(pib^2 + qib^2 + 1) - qij / sqrt(pij^2 + qij^2 +1))*/
		else if (kPlus1 % 11 == 10)
		{
			Fij = Qij / NORM_PQ(Pij, Qij);
			Fib = Qib / NORM_PQ(Pib, Qib);
			hx[k] = (Fib - Fij);
		}
		/*f(pij, qij, pib, qib) = (-1 / sqrt(pib^2 + qib^2 + 1) + 1 / sqrt(pij^2 + qij^2 +1))*/
		else if (kPlus1 % 11 == 0)
		{
			Fij = -1 / NORM_PQ(Pij, Qij);
			Fib = -1 / NORM_PQ(Pib, Qib);
			hx[k] = (Fib - Fij);
		}
		///*f(pij, qij, qaj, pib) = (pij - qij + qaj - pib)*/
		//else if (kPlus1 % 10 == 0)
		//{
		//	Qaj = p[qBottomIndex];
		//	Pib = p[pRightIndex];
		//	hx[k] = (Pij - Qij + Qaj - Pib);
		//}
	}
}

void OptimizingCostAnjacCRS(double *p, struct splm_crsm *jac, int m, int n, void *adata)
{
	register int currentIndex, pIndex, qIndex, pBottomIndex, qBottomIndex,
		pRightIndex, qRightIndex, k, kPlus1;
	register int currentJacIndex = 0;

	for (k = 0; k < n; k++) {
		jac->rowptr[k] = currentJacIndex;

		kPlus1 = k + 1;
		int ParamIndexs_OPT_ENERGY_Index = DIV(k, 11);
		currentIndex = ParamIndexs_OPT_ENERGY[ParamIndexs_OPT_ENERGY_Index];

		pIndex = 2 * currentIndex;
		qIndex = pIndex + 1;
		pBottomIndex = connectIndex_OPT_ENERGY[currentIndex][0];
		qBottomIndex = pBottomIndex + 1;
		pRightIndex = connectIndex_OPT_ENERGY[currentIndex][1];
		qRightIndex = pRightIndex + 1;

		if (kPlus1 % 11 == 1) //Diff(Saj - Sij)
		{
			double rij = paramRij_OPT_ENERGY[currentIndex][0];
			double raj = paramRij_OPT_ENERGY[currentIndex][1];
			/* Diff fij */
			// Diff(f(Pij, Paj, Qij, Qaj), Pij)
			double diffPij = 0;
			if (!ifZero_OPT_ENERGY[pIndex])
				diffPij += (-1) * rij * ComputeFunctionEgradDiffPart_P(p[pIndex], p[qIndex]);
			jac->val[currentJacIndex] = diffPij;
			jac->colidx[currentJacIndex] = pIndex;
			currentJacIndex++;
			// Diff(f(Pij, Paj, Qij, Qaj), Qij)
			double diffQij = 0;
			if (!ifZero_OPT_ENERGY[qIndex])
				diffQij += 1 * rij * ComputeFunctionEgradDiffPart_Q(p[pIndex], p[qIndex]);
			jac->val[currentJacIndex] = diffQij;
			jac->colidx[currentJacIndex] = qIndex;
			currentJacIndex++;
			/* Diff faj */
			// Diff(f(Pij, Paj, Qij, Qaj), Paj)
			double diffPaj = 0;
			if (!ifZero_OPT_ENERGY[pBottomIndex])
				diffPaj += 1 * raj * ComputeFunctionEgradDiffPart_P(p[pBottomIndex], p[qBottomIndex]);
			jac->val[currentJacIndex] = diffPaj;
			jac->colidx[currentJacIndex] = pBottomIndex;
			currentJacIndex++;
			// Diff(f(Pij, Paj, Qij, Qaj), Qaj)
			double diffQaj = 0;
			if (!ifZero_OPT_ENERGY[qBottomIndex])
				diffQaj += (-1) * raj * ComputeFunctionEgradDiffPart_Q(p[pBottomIndex], p[qBottomIndex]);
			jac->val[currentJacIndex] = diffQaj;
			jac->colidx[currentJacIndex] = qBottomIndex;
			currentJacIndex++;
		}
		else if (kPlus1 % 11 == 2) //Diff(Sib - Sij)
		{
			double rij = paramRij_OPT_ENERGY[currentIndex][0];
			double rib = paramRij_OPT_ENERGY[currentIndex][2];
			/* Diff fij */
			// Diff(f(Pij, Paj, Qij, Qaj), Pij)
			double diffPij = 0;
			if (!ifZero_OPT_ENERGY[pIndex])
				diffPij += (-1) * rij * ComputeFunctionEgradDiffPart_P(p[pIndex], p[qIndex]);
			jac->val[currentJacIndex] = diffPij;
			jac->colidx[currentJacIndex] = pIndex;
			currentJacIndex++;
			// Diff(f(Pij, Paj, Qij, Qaj), Qij)
			double diffQij = 0;
			if (!ifZero_OPT_ENERGY[qIndex])
				diffQij += 1 * rij * ComputeFunctionEgradDiffPart_Q(p[pIndex], p[qIndex]);
			jac->val[currentJacIndex] = diffQij;
			jac->colidx[currentJacIndex] = qIndex;
			currentJacIndex++;
			/* Diff fib */
			// Diff(f(Pij, Pib, Qij, Qib), Pib)
			double diffPib = 0;
			if (!ifZero_OPT_ENERGY[pRightIndex])
				diffPib += 1 * rib * ComputeFunctionEgradDiffPart_P(p[pRightIndex], p[qRightIndex]);
			jac->val[currentJacIndex] = diffPib;
			jac->colidx[currentJacIndex] = pRightIndex;
			currentJacIndex++;
			// Diff(f(Pij, Pib, Qij, Qib), Qib)
			double diffQib = 0;
			if (!ifZero_OPT_ENERGY[qRightIndex])
				diffQib += (-1) * rib * ComputeFunctionEgradDiffPart_Q(p[pRightIndex], p[qRightIndex]);
			jac->val[currentJacIndex] = diffQib;
			jac->colidx[currentJacIndex] = qRightIndex;
			currentJacIndex++;
		}
		else if (kPlus1 % 11 == 3) //Diff(pij / sqrt(pij^2 + qij^2 + 1)
		{
			double Pij = p[pIndex], Qij = p[qIndex];
			double fijNorm = NORM_PQ(Pij, Qij);
			double fijNorm3 = fijNorm * fijNorm * fijNorm;
			/* Diff fij */
			// Diff(f(Pij, Qij), Pij) ****** f(Pij, Qij) = Pij / sqrt(Pij^2 + Qij^2 + 1)
			double diffPij = (1 / fijNorm) - Pij * Pij / fijNorm3;
			jac->val[currentJacIndex] = diffPij;
			jac->colidx[currentJacIndex] = pIndex;
			currentJacIndex++;
			// Diff(f(Pij, Qij), Qij) ****** f(Pij, Qij) = Pij / sqrt(Pij^2 + Qij^2 + 1)
			double diffQij = (-1) * (Pij * Qij) / fijNorm3;
			jac->val[currentJacIndex] = diffQij;
			jac->colidx[currentJacIndex] = qIndex;
			currentJacIndex++;
		}
		else if (kPlus1 % 11 == 4) //Diff(qij / sqrt(pij^2 + qij^2 + 1)
		{
			double Pij = p[pIndex], Qij = p[qIndex];
			double fijNorm = NORM_PQ(Pij, Qij);
			double fijNorm3 = fijNorm * fijNorm * fijNorm;
			/* Diff fij */
			// Diff(f(Pij, Qij), Pij) ****** f(Pij, Qij) = Qij / sqrt(Pij^2 + Qij^2 + 1)
			double diffPij = (-1) * (Pij * Qij) / fijNorm3;
			jac->val[currentJacIndex] = diffPij;
			jac->colidx[currentJacIndex] = pIndex;
			currentJacIndex++;
			// Diff(f(Pij, Qij), Qij) ****** f(Pij, Qij) = Qij / sqrt(Pij^2 + Qij^2 + 1)
			double diffQij = (1 / fijNorm) - Qij * Qij / fijNorm3;
			jac->val[currentJacIndex] = diffQij;
			jac->colidx[currentJacIndex] = qIndex;
			currentJacIndex++;
		}
		else if (kPlus1 % 11 == 5) //Diff(-1 / sqrt(pij^2 + qij^2 + 1)
		{
			double Pij = p[pIndex], Qij = p[qIndex];
			double fijNorm = NORM_PQ(Pij, Qij);
			double fijNorm3 = fijNorm * fijNorm * fijNorm;
			/* Diff fij */
			// Diff(f(Pij, Qij), Pij) ****** f(Pij, Qij) = -1 / sqrt(Pij^2 + Qij^2 + 1)
			double diffPij = Pij / fijNorm3;
			jac->val[currentJacIndex] = diffPij;
			jac->colidx[currentJacIndex] = pIndex;
			currentJacIndex++;
			// Diff(f(Pij, Qij), Qij) ****** f(Pij, Qij) = -1 / sqrt(Pij^2 + Qij^2 + 1)
			double diffQij = Qij / fijNorm3;
			jac->val[currentJacIndex] = diffQij;
			jac->colidx[currentJacIndex] = qIndex;
			currentJacIndex++;
		}
		/*Diff((paj / sqrt(paj^2 + qaj^2 + 1) - pij / sqrt(pij^2 + qij^2 +1)))*/
		else if (kPlus1 % 11 == 6)
		{
			double Pij = p[pIndex], Qij = p[qIndex];
			double fijNorm = NORM_PQ(Pij, Qij);
			double fijNorm3 = fijNorm * fijNorm * fijNorm;
			/* Diff fij */
			// Diff(f(Pij, Paj, Qij, Qaj), Pij)
			double diffPij = (-1) * ((1 / fijNorm) - Pij * Pij / fijNorm3);
			jac->val[currentJacIndex] = diffPij;
			jac->colidx[currentJacIndex] = pIndex;
			currentJacIndex++;
			// Diff(f(Pij, Paj, Qij, Qaj), Qij)
			double diffQij = (Pij * Qij) / fijNorm3;
			jac->val[currentJacIndex] = diffQij;
			jac->colidx[currentJacIndex] = qIndex;
			currentJacIndex++;
			/* Diff faj */
			double Paj = p[pBottomIndex], Qaj = p[qBottomIndex];
			double fajNorm = NORM_PQ(Paj, Qaj);
			double fajNorm3 = fajNorm * fajNorm * fajNorm;
			/* Diff faj */
			// Diff(f(Pij, Paj, Qij, Qaj), Paj)
			double diffPaj = (1 / fajNorm) - Paj * Paj / fajNorm3;
			jac->val[currentJacIndex] = diffPaj;
			jac->colidx[currentJacIndex] = pBottomIndex;
			currentJacIndex++;
			// Diff(f(Pij, Paj, Qij, Qaj), Qaj)
			double diffQaj = (-1) * (Paj * Qaj) / fajNorm3;
			jac->val[currentJacIndex] = diffQaj;
			jac->colidx[currentJacIndex] = qBottomIndex;
			currentJacIndex++;
		}
		/*Diff((qaj / sqrt(paj^2 + qaj^2 + 1) - qij / sqrt(pij^2 + qij^2 +1)))*/
		else if (kPlus1 % 11 == 7)
		{
			double Pij = p[pIndex], Qij = p[qIndex];
			double fijNorm = NORM_PQ(Pij, Qij);
			double fijNorm3 = fijNorm * fijNorm * fijNorm;
			/* Diff fij */
			// Diff(f(Pij, Paj, Qij, Qaj), Pij)
			double diffPij = (Pij * Qij) / fijNorm3;
			jac->val[currentJacIndex] = diffPij;
			jac->colidx[currentJacIndex] = pIndex;
			currentJacIndex++;
			// Diff(f(Pij, Paj, Qij, Qaj), Qij)
			double diffQij = (-1) * (1 / fijNorm) - Qij * Qij / fijNorm3;
			jac->val[currentJacIndex] = diffQij;
			jac->colidx[currentJacIndex] = qIndex;
			currentJacIndex++;
			/* Diff faj */
			double Paj = p[pBottomIndex], Qaj = p[qBottomIndex];
			double fajNorm = NORM_PQ(Paj, Qaj);
			double fajNorm3 = fajNorm * fajNorm * fajNorm;
			/* Diff faj */
			// Diff(f(Pij, Paj, Qij, Qaj), Paj)
			double diffPaj = (-1) * (Paj * Qaj) / fajNorm3;
			jac->val[currentJacIndex] = diffPaj;
			jac->colidx[currentJacIndex] = pBottomIndex;
			currentJacIndex++;
			// Diff(f(Pij, Paj, Qij, Qaj), Qaj)
			double diffQaj = (1 / fajNorm) - Qaj * Qaj / fajNorm3;
			jac->val[currentJacIndex] = diffQaj;
			jac->colidx[currentJacIndex] = qBottomIndex;
			currentJacIndex++;
		}
		/*Diff((-1 / sqrt(paj^2 + qaj^2 + 1) + 1 / sqrt(pij^2 + qij^2 +1)))*/
		else if (kPlus1 % 11 == 8)
		{
			double Pij = p[pIndex], Qij = p[qIndex];
			double fijNorm = NORM_PQ(Pij, Qij);
			double fijNorm3 = fijNorm * fijNorm * fijNorm;
			/* Diff fij */
			// Diff(f(Pij, Paj, Qij, Qaj), Pij)
			double diffPij = (-1) * Pij / fijNorm3;
			jac->val[currentJacIndex] = diffPij;
			jac->colidx[currentJacIndex] = pIndex;
			currentJacIndex++;
			// Diff(f(Pij, Paj, Qij, Qaj), Qij)
			double diffQij = (-1) * Qij / fijNorm3;
			jac->val[currentJacIndex] = diffQij;
			jac->colidx[currentJacIndex] = qIndex;
			currentJacIndex++;
			/* Diff faj */
			double Paj = p[pBottomIndex], Qaj = p[qBottomIndex];
			double fajNorm = NORM_PQ(Paj, Qaj);
			double fajNorm3 = fajNorm * fajNorm * fajNorm;
			/* Diff faj */
			// Diff(f(Pij, Paj, Qij, Qaj), Paj)
			double diffPaj = Paj / fajNorm3;
			jac->val[currentJacIndex] = diffPaj;
			jac->colidx[currentJacIndex] = pBottomIndex;
			currentJacIndex++;
			// Diff(f(Pij, Paj, Qij, Qaj), Qaj)
			double diffQaj = Qaj / fajNorm3;
			jac->val[currentJacIndex] = diffQaj;
			jac->colidx[currentJacIndex] = qBottomIndex;
			currentJacIndex++;
		}
		/*Diff((pib / sqrt(pib^2 + qib^2 + 1) - pij / sqrt(pij^2 + qij^2 +1)))*/
		else if (kPlus1 % 11 == 9)
		{
			double Pij = p[pIndex], Qij = p[qIndex];
			double fijNorm = NORM_PQ(Pij, Qij);
			double fijNorm3 = fijNorm * fijNorm * fijNorm;
			/* Diff fij */
			// Diff(f(Pij, Pib, Qij, Qib), Pij)
			double diffPij = (-1) * ((1 / fijNorm) - Pij * Pij / fijNorm3);
			jac->val[currentJacIndex] = diffPij;
			jac->colidx[currentJacIndex] = pIndex;
			currentJacIndex++;
			// Diff(f(Pij, Pib, Qij, Qib), Qij)
			double diffQij = (Pij * Qij) / fijNorm3;
			jac->val[currentJacIndex] = diffQij;
			jac->colidx[currentJacIndex] = qIndex;
			currentJacIndex++;
			/* Diff fib */
			double Pib = p[pRightIndex], Qib = p[qRightIndex];
			double fibNorm = NORM_PQ(Pib, Qib);
			double fibNorm3 = fibNorm * fibNorm * fibNorm;
			/* Diff fib */
			// Diff(f(Pij, Pib, Qij, Qib), Pib)
			double diffPib = (1 / fibNorm) - Pib * Pib / fibNorm3;
			jac->val[currentJacIndex] = diffPib;
			jac->colidx[currentJacIndex] = pRightIndex;
			currentJacIndex++;
			// Diff(f(Pij, Pib, Qij, Qib), Qib)
			double diffQib = (-1) * (Pib * Qib) / fibNorm3;
			jac->val[currentJacIndex] = diffQib;
			jac->colidx[currentJacIndex] = qRightIndex;
			currentJacIndex++;
		}
		/*Diff((qib / sqrt(pib^2 + qib^2 + 1) - qij / sqrt(pij^2 + qij^2 +1)))*/
		else if (kPlus1 % 11 == 10)
		{
			double Pij = p[pIndex], Qij = p[qIndex];
			double fijNorm = NORM_PQ(Pij, Qij);
			double fijNorm3 = fijNorm * fijNorm * fijNorm;
			/* Diff fij */
			// Diff(f(Pij, Pib, Qij, Qib), Pij)
			double diffPij = (Pij * Qij) / fijNorm3;
			jac->val[currentJacIndex] = diffPij;
			jac->colidx[currentJacIndex] = pIndex;
			currentJacIndex++;
			// Diff(f(Pij, Pib, Qij, Qib), Qij)
			double diffQij = (-1) * (1 / fijNorm) - Qij * Qij / fijNorm3;
			jac->val[currentJacIndex] = diffQij;
			jac->colidx[currentJacIndex] = qIndex;
			currentJacIndex++;
			/* Diff fib */
			double Pib = p[pRightIndex], Qib = p[qRightIndex];
			double fibNorm = NORM_PQ(Pib, Qib);
			double fibNorm3 = fibNorm * fibNorm * fibNorm;
			/* Diff fib */
			// Diff(f(Pij, Pib, Qij, Qib), Pib)
			double diffPib = (-1) * (Pib * Qib) / fibNorm3;
			jac->val[currentJacIndex] = diffPib;
			jac->colidx[currentJacIndex] = pRightIndex;
			currentJacIndex++;
			// Diff(f(Pij, Pib, Qij, Qib), Qib)
			double diffQib = (1 / fibNorm) - Qib * Qib / fibNorm3;
			jac->val[currentJacIndex] = diffQib;
			jac->colidx[currentJacIndex] = qRightIndex;
			currentJacIndex++;
		}
		/*Diff((-1 / sqrt(pib^2 + qib^2 + 1) + 1 / sqrt(pij^2 + qij^2 +1)))*/
		else if (kPlus1 % 11 == 0)
		{
			double Pij = p[pIndex], Qij = p[qIndex];
			double fijNorm = NORM_PQ(Pij, Qij);
			double fijNorm3 = fijNorm * fijNorm * fijNorm;
			/* Diff fij */
			// Diff(f(Pij, Pib, Qij, Qib), Pij)
			double diffPij = (-1) * Pij / fijNorm3;
			jac->val[currentJacIndex] = diffPij;
			jac->colidx[currentJacIndex] = pIndex;
			currentJacIndex++;
			// Diff(f(Pij, Pib, Qij, Qib), Qij)
			double diffQij = (-1) * Qij / fijNorm3;
			jac->val[currentJacIndex] = diffQij;
			jac->colidx[currentJacIndex] = qIndex;
			currentJacIndex++;
			/* Diff fib */
			double Pib = p[pRightIndex], Qib = p[qRightIndex];
			double fibNorm = NORM_PQ(Pib, Qib);
			double fibNorm3 = fibNorm * fibNorm * fibNorm;
			/* Diff fib */
			// Diff(f(Pij, Pib, Qij, Qib), Pib)
			double diffPib = Pib / fibNorm3;
			jac->val[currentJacIndex] = diffPib;
			jac->colidx[currentJacIndex] = pRightIndex;
			currentJacIndex++;
			// Diff(f(Pij, Pib, Qij, Qib), Qib)
			double diffQib = Qib / fibNorm3;
			jac->val[currentJacIndex] = diffQib;
			jac->colidx[currentJacIndex] = qRightIndex;
			currentJacIndex++;
		}
		///*Diff(pij - qij + qaj - pib)*/
		//else if (kPlus1 % 10 == 0)
		//{
		//	/*Diff(f(pij, qij, qaj, pib), pij)*/
		//	jac->val[currentJacIndex] = 1;
		//	jac->colidx[currentJacIndex] = pIndex;
		//	currentJacIndex++;
		//	/*Diff(f(pij, qij, qaj, pib), qij)*/
		//	jac->val[currentJacIndex] = -1;
		//	jac->colidx[currentJacIndex] = qIndex;
		//	currentJacIndex++;
		//	/*Diff(f(pij, qij, qaj, pib), qaj)*/
		//	jac->val[currentJacIndex] = 1;
		//	jac->colidx[currentJacIndex] = qBottomIndex;
		//	currentJacIndex++;
		//	/*Diff(f(pij, qij, qaj, pib), pib)*/
		//	jac->val[currentJacIndex] = -1;
		//	jac->colidx[currentJacIndex] = pRightIndex;
		//	currentJacIndex++;
		//}
	}
	jac->rowptr[n] = currentJacIndex;
}


FaceSFS::FaceSFS(cv::Mat rgbImage, MyMesh* myMesh, WeightSFS weight)
{
	if (rgbImage.channels() != 3)
	{
		printf("The current image isn't a RGB image!\n");
		return;
	}

	cv::cvtColor(rgbImage, mGrayImg, CV_RGB2GRAY);

	resultShowOne = mGrayImg.clone();

	resultShowThree = rgbImage.clone();

	mHeight = rgbImage.rows;
	mWidth = rgbImage.cols;

	/*Set Mesh*/
	mMesh = myMesh;
	ScalingMesh(mMesh);

	/*Set weight*/
	mWeight.u1 = weight.u1;
	mWeight.u2 = weight.u2;
	mWeight.u3 = weight.u3;

	/*Alloc memory*/
	//Depth
	mDepths = new double*[mHeight];
	//Image Mark
	mImgMark = new bool*[mHeight];
	//Mesh normal
	mMeshNormals = new SFSPoint*[mHeight];
	//Image's intensity
	mImgIntensity = new double*[mHeight];
	//Face's albedos
	mAlbedos = new double*[mHeight];
	//Gradient mark
	mGradientMark = new bool*[mHeight];
	//The gradient of depth
	mPQs = new IntegralPQ*[mHeight];
	//The Image normal
	mImgNormals = new SFSPoint*[mHeight];
	//Face OPT Mark
	mFaceOPTMark = new bool*[mHeight];
	for (int i = 0; i < mHeight; i++)
	{
		//Depth
		mDepths[i] = new double[mWidth];
		//Image Mark
		mImgMark[i] = new bool[mWidth];
		//Mesh normal
		mMeshNormals[i] = new SFSPoint[mWidth];
		//Image's intensity
		mImgIntensity[i] = new double[mWidth];
		//Face's albedos
		mAlbedos[i] = new double[mWidth];
		//Gradient mark
		mGradientMark[i] = new bool[mWidth];
		//The gradient of depth
		mPQs[i] = new IntegralPQ[mWidth];
		//The Image normal
		mImgNormals[i] = new SFSPoint[mWidth];
		//Face OPT Mark
		mFaceOPTMark[i] = new bool[mWidth];
	}
	/*Init*/
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			//Init image's face mark
			mImgMark[i][j] = false;
			//Init image's intensity
			mImgIntensity[i][j] = mGrayImg.ptr<uchar>(i)[j];
			//Init gradient mark
			mGradientMark[i][j] = false;
			//Init face OPT mark
			mFaceOPTMark[i][j] = false;
		}
	}
}

FaceSFS::~FaceSFS()
{
	for (int i = 0; i < mHeight; i++)
	{
		delete[] mDepths[i];
		delete[] mImgMark[i];
		delete[] mMeshNormals[i];
		delete[] mImgIntensity[i];
		delete[] mAlbedos[i];
		delete[] mGradientMark[i];
		delete[] mPQs[i];
		delete[] mImgNormals[i];
		delete[] mFaceOPTMark[i];
	}
	delete[] mDepths;
	delete[] mImgMark;
	delete[] mMeshNormals;
	delete[] mImgIntensity;
	delete[] mAlbedos;
	delete[] mGradientMark;
	delete[] mPQs;
	delete[] mImgNormals;
	delete[] mFaceOPTMark;
}

void FaceSFS::ShapeFromShading()
{
	FindFace_ComputeNormal_GetDepth();

	for (int i = 0; i < mHeight; i++)
	{
		uchar* currentPixel = resultShowThree.ptr<uchar>(i);
		for (int j = 0; j < mWidth; j++)
		{
			if (mImgMark[i][j])
			{
				int index = j * 3;
				currentPixel[index] = abs(mMeshNormals[i][j][0]) * 100;
				currentPixel[index + 1] = abs(mMeshNormals[i][j][1]) * 100;
				currentPixel[index + 2] = abs(mMeshNormals[i][j][2]) * 100;
			}
			else
			{
				int index = j * 3;
				currentPixel[index] = 0;
				currentPixel[index + 1] = 0;
				currentPixel[index + 2] = 0;
			}
		}
	}

	for (int i = 0; i < mHeight; i++)
	{
		uchar* currentPixel = resultShowOne.ptr<uchar>(i);
		for (int j = 0; j < mWidth; j++)
		{
			if (!mImgMark[i][j])
			{
				currentPixel[j] = 0;
			}
			if (mImgMark[i][j])
			{
				currentPixel[j] = mDepths[i][j] + 50.0f;
			}
		}
	}

	cv::imshow("resultShowOne", resultShowOne);

	cv::imshow("resultShowThree", resultShowThree);
	cv::waitKey();

	ComputeAlbedos();

	ComputePQ();
	for (int i = 0; i < mHeight; i++)
	{
		uchar* currentPixel = resultShowThree.ptr<uchar>(i);
		for (int j = 0; j < mWidth; j++)
		{
			int index = j * 3;
			currentPixel[index] = abs(mPQs[i][j].mP * 100);
			currentPixel[index + 1] = abs(mPQs[i][j].mQ * 100);
			currentPixel[index + 2] = 0;
		}
	}
	cv::imshow("resultShowThree", resultShowThree);
	cv::waitKey();

	ComputeImgNormal();
	for (int i = 0; i < mHeight; i++)
	{
		uchar* currentPixel = resultShowThree.ptr<uchar>(i);
		for (int j = 0; j < mWidth; j++)
		{
			int index = j * 3;
			currentPixel[index] = abs(mImgNormals[i][j][0] * 100);
			currentPixel[index + 1] = abs(mImgNormals[i][j][1] * 100);
			currentPixel[index + 2] = abs(mImgNormals[i][j][2] * 100);
		}
	}
	cv::imshow("resultShowThree", resultShowThree);
	cv::waitKey();


	//x (260,560) y (345, 440)
	SelectingRange(340, 400, 300, 500);

	OptimizingPQ();

	for (int i = 0; i < mHeight; i++)
	{
		uchar* currentPixel = resultShowThree.ptr<uchar>(i);
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceOPTMark[i][j])
			{
				int index = j * 3;
				currentPixel[index] = abs(mPQs[i][j].mP * 100);
				currentPixel[index + 1] = abs(mPQs[i][j].mQ * 100);
				currentPixel[index + 2] = 0;
			}
			else
			{
				int index = j * 3;
				currentPixel[index] = abs(mPQs[i][j].mP * 100);
				currentPixel[index + 1] = abs(mPQs[i][j].mQ * 100);
				currentPixel[index + 2] = 0;
			}
		}
	}
	cv::imshow("resultShowThree", resultShowThree);
	cv::waitKey();
}

void FaceSFS::FindFace_ComputeNormal_GetDepth()
{
	/*Find the face range and compute the mesh's normal in pixels range and get the mesh's depth*/
	for (MyMesh::FPtr pF : It::MFIterator(mMesh))
	{
		SFSPoint currentP[3];
		int pNum = 0;

		double dU = 0;
		double dV = 0;

		double myDepth = 0;
		for (MyMesh::VPtr pV : It::FVIterator(pF))
		{
			currentP[pNum] = pV->point();
			pNum++;

			dU += pV->uv()[0] * mWidth;
			dV += pV->uv()[1] * mHeight;

			myDepth += pV->point()[2];
		}
		int iU = dU / 3;
		int iV = mHeight - dV / 3; ////////////////////////////////////////////////////////////////////error!!!!!!!!!

		if (iU > 0 && iU < (mWidth - 1) && iV > 0 && iV < (mHeight - 1))
			mImgMark[iV][iU] = true;

		SFSPoint faceNormal = (currentP[1] - currentP[0]) ^ (currentP[2] - currentP[0]);

		mMeshNormals[iV][iU] = faceNormal / faceNormal.norm();

		myDepth /= 3;

		mDepths[iV][iU] = myDepth;
	}

	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mImgMark[i][j]) {
				int offsetNum = 5;
				int offset[4] = { 0 };
				for (int k = 1; k <= offsetNum; k++)
				{
					if ((i - k) > 0)
						if (mImgMark[i - k][j])
							offset[0] = k;
					if ((i + k) < mHeight - 1)
						if (mImgMark[i + k][j])
							offset[1] = k;
					if ((j - k) > 0)
						if (mImgMark[i][j - k])
							offset[2] = k;
					if ((j + k) < mWidth - 1)
						if (mImgMark[i][j + k])
							offset[3] = k;
				}
				for (int k = 1; k < offset[0]; k++)
				{
					int maxOffset = offset[0];

					mImgMark[i - k][j] = true;

					SFSPoint normalDiff;
					normalDiff = ((mMeshNormals[i - maxOffset][j] - mMeshNormals[i][j]) * k / maxOffset);
					mMeshNormals[i - k][j] = mMeshNormals[i][j] + normalDiff;

					double depthDiff = ((mDepths[i - maxOffset][j] - mDepths[i][j]) * k / maxOffset);
					mDepths[i - k][j] = mDepths[i][j] + depthDiff;
				}
				for (int k = 1; k < offset[1]; k++)
				{
					int maxOffset = offset[1];

					mImgMark[i + k][j] = true;

					SFSPoint normalDiff;
					normalDiff = ((mMeshNormals[i + maxOffset][j] - mMeshNormals[i][j]) * k / maxOffset);
					mMeshNormals[i + k][j] = mMeshNormals[i][j] + normalDiff;

					double depthDiff = ((mDepths[i + maxOffset][j] - mDepths[i][j]) * k / maxOffset);
					mDepths[i + k][j] = mDepths[i][j] + depthDiff;
				}
				for (int k = 1; k < offset[2]; k++)
				{
					int maxOffset = offset[2];

					mImgMark[i][j - k] = true;

					SFSPoint normalDiff;
					normalDiff = ((mMeshNormals[i][j - maxOffset] - mMeshNormals[i][j]) * k / maxOffset);
					mMeshNormals[i][j - k] = mMeshNormals[i][j] + normalDiff;

					double depthDiff = ((mDepths[i][j - maxOffset] - mDepths[i][j]) * k / maxOffset);
					mDepths[i][j - k] = mDepths[i][j] + depthDiff;
				}
				for (int k = 1; k < offset[3]; k++)
				{
					int maxOffset = offset[3];

					mImgMark[i][j + k] = true;

					SFSPoint normalDiff;
					normalDiff = ((mMeshNormals[i][j + maxOffset] - mMeshNormals[i][j]) * k / maxOffset);
					mMeshNormals[i][j + k] = mMeshNormals[i][j] + normalDiff;

					double depthDiff = ((mDepths[i][j + maxOffset] - mDepths[i][j]) * k / maxOffset);
					mDepths[i][j + k] = mDepths[i][j] + depthDiff;
				}
			}
		}
	}

	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mImgMark[i][j])
			{
				if (mImgMark[i + 1][j] && mImgMark[i - 1][j] && mImgMark[i][j + 1] && mImgMark[i][j - 1])
				{
					mFaceOPTMark[i][j] = true;
					mFaceOPTMark[i - 1][j] = true;
					mFaceOPTMark[i + 1][j] = true;
					mFaceOPTMark[i][j - 1] = true;
					mFaceOPTMark[i][j + 1] = true;
				}
			}
		}
	}

	mFaceSize = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceOPTMark[i][j])
				mFaceSize++;
		}
	}
}

void FaceSFS::SelectingRange(int startRow, int endRow, int startCol, int endCol)
{
	for (int i = 0; i < mHeight; i++)
		for (int j = 0; j < mWidth; j++)
			mFaceOPTMark[i][j] = false;

	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mGradientMark[i][j])
			{
				if (!mGradientMark[i - 1][j] && !mGradientMark[i + 1][j] && !mGradientMark[i][j - 1] && !mGradientMark[i][j + 1])
					mGradientMark[i][j] = false;
				if (i < startRow || i > endRow || j < startCol || j > endCol)
					mGradientMark[i][j] = false;
			}
		}
	}
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mGradientMark[i][j])
			{
				if (mGradientMark[i + 1][j] && mGradientMark[i][j + 1])
				{
					mFaceOPTMark[i][j] = true;
					mFaceOPTMark[i + 1][j] = true;
					mFaceOPTMark[i][j + 1] = true;
				}
			}
		}
	}

	mFaceSize = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceOPTMark[i][j])
				mFaceSize++;
		}
	}
}

void FaceSFS::ComputeAlbedos()
{
	/*Init the Albedos*/
	double totalPixelsValue = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceOPTMark[i][j])
			{
				totalPixelsValue += mImgIntensity[i][j];
			}
		}
	}
	double initAlbedo = totalPixelsValue / mFaceSize;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceOPTMark[i][j])
				mAlbedos[i][j] = initAlbedo;
			else
				mAlbedos[i][j] = 0;
		}
	}
	/*Init spherical harmonics*/
	mParamRij_OPT_HE = new double[mFaceSize];
	mVectorSH_OPT_HE = new VectorSH[mFaceSize];
	mParamIij_OPT_HE = new double[mFaceSize];
	int nowIndex = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceOPTMark[i][j])
			{
				mParamRij_OPT_HE[nowIndex] = mAlbedos[i][j];

				mParamIij_OPT_HE[nowIndex] = mImgIntensity[i][j];

				double Nx = mMeshNormals[i][j][0];
				double Ny = mMeshNormals[i][j][1];
				double Nz = mMeshNormals[i][j][2];
				mVectorSH_OPT_HE[nowIndex].mH[0] = 1;					//1
				mVectorSH_OPT_HE[nowIndex].mH[1] = Nx;					//Nx
				mVectorSH_OPT_HE[nowIndex].mH[2] = Ny;					//Ny
				mVectorSH_OPT_HE[nowIndex].mH[3] = Nz;					//Nz
				mVectorSH_OPT_HE[nowIndex].mH[4] = Nx * Ny;			//Nx * Ny
				mVectorSH_OPT_HE[nowIndex].mH[5] = Nx * Nz;			//Nx * Nz
				mVectorSH_OPT_HE[nowIndex].mH[6] = Ny * Nz;			//Ny * Nz
				mVectorSH_OPT_HE[nowIndex].mH[7] = Nx * Nx - Ny * Ny;	//Nx * Nx - Ny * Ny
				mVectorSH_OPT_HE[nowIndex].mH[8] = 3 * Nz * Nz - 1;	//3 * Nz * Nz - 1
				nowIndex++;
			}
		}
	}
	/*Fixing the albedos and pixels corrective values to optimize the harmonic coefficients*/
	const int heParamsNum = 9;
	const int heMeasurementsNum = mFaceSize;

	double* heParams = new double[9];

	for (int i = 0; i < 9; i++)
		heParams[i] = 0.5f;

	double opts_LEVMAR[LM_OPTS_SZ], info_LEVMAR[LM_INFO_SZ];

	opts_LEVMAR[0] = LM_INIT_MU; opts_LEVMAR[1] = 1E-15; opts_LEVMAR[2] = 1E-15; opts_LEVMAR[3] = 1E-20, opts_LEVMAR[4] = LM_DIFF_DELTA;

	int ret;
	ret = dlevmar_der(OptimizingHECost, OptimizingHECostJac, heParams, NULL, heParamsNum, heMeasurementsNum, 1000, opts_LEVMAR, info_LEVMAR, NULL, NULL, NULL);

	for (int i = 0; i < 9; i++)
	{
		mVectorHE.mE[i] = heParams[i];
		mHE_OPT_PCV[i] = heParams[i];
	}
	/*Init the pixels corrective value*/
	double** mPCVs;
	mPCVs = new double*[mHeight];
	for (int i = 0; i < mHeight; i++)
	{
		mPCVs[i] = new double[mWidth];
		for (int j = 0; j < mWidth; j++)
		{
			mPCVs[i][j] = 0.1f;
		}
	}
	//*Fixing the albedos and the harmonic coefficients to optimize the pixels corrective values*/
	mConnectIndex_OPT_PCV = new int*[mFaceSize];
	for (int i = 0; i < mFaceSize; i++)
	{
		mConnectIndex_OPT_PCV[i] = new int[4];
	}
	int** connectMap = new int*[mHeight];
	for (int i = 0; i < mHeight; i++)
	{
		connectMap[i] = new int[mWidth];
		for (int j = 0; j < mWidth; j++)
			connectMap[i][j] = -1;
	}

	double opts_SPLM[SPLM_OPTS_SZ], info_SPLM[SPLM_INFO_SZ];
	opts_SPLM[0] = SPLM_INIT_MU;
	opts_SPLM[1] = SPLM_STOP_THRESH;
	opts_SPLM[2] = SPLM_STOP_THRESH;
	opts_SPLM[3] = SPLM_STOP_THRESH;
	opts_SPLM[4] = SPLM_DIFF_DELTA;
	opts_SPLM[5] = SPLM_CHOLMOD;

	int nnz = 0;

	double* pcvParams = new double[mFaceSize];
	int pcvMeasurementsNum = 0;

	nowIndex = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceOPTMark[i][j])
			{
				pcvParams[nowIndex] = mPCVs[i][j];

				connectMap[i][j] = nowIndex;

				nowIndex++;
			}
		}
	}

	nowIndex = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceOPTMark[i][j])
			{
				mConnectIndex_OPT_PCV[nowIndex][0] = connectMap[i - 1][j];
				mConnectIndex_OPT_PCV[nowIndex][1] = connectMap[i + 1][j];
				mConnectIndex_OPT_PCV[nowIndex][2] = connectMap[i][j - 1];
				mConnectIndex_OPT_PCV[nowIndex][3] = connectMap[i][j + 1];

				if (mConnectIndex_OPT_PCV[nowIndex][0] != -1 && mConnectIndex_OPT_PCV[nowIndex][1] != -1
					&& mConnectIndex_OPT_PCV[nowIndex][2] != -1 && mConnectIndex_OPT_PCV[nowIndex][3] != -1)
				{
					ParamIndexs_OPT_PCV.push_back(nowIndex);
				}
				nowIndex++;
			}
		}
	}

	nowIndex = 0;
	int ParamIndexs_OPT_PCV_Index = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceOPTMark[i][j])
			{
				if (nowIndex == ParamIndexs_OPT_PCV[ParamIndexs_OPT_PCV_Index])
				{
					/*f(dij) = rij * (HE * EH + dij) - Iij*/
					pcvMeasurementsNum++;
					nnz += 1;
					/*f(dij) = dij*/
					pcvMeasurementsNum++;
					nnz += 1;
					/*f(daj, dij) = (daj - dij)*/
					pcvMeasurementsNum++;
					nnz += 2;
					/*f(dib, dij) = dib - dij*/
					pcvMeasurementsNum++;
					nnz += 2;
					/*f(dij, daj, dib, dcj, did) = 4*dij - (daj + dib + dcj + did) */
					pcvMeasurementsNum++;
					nnz += 5;

					ParamIndexs_OPT_PCV_Index++;
				}
				nowIndex++;
			}
		}
	}

	cout << "ParamIndexs_OPT_PCV_Index:" << ParamIndexs_OPT_PCV_Index << "," << ParamIndexs_OPT_PCV.size() << endl;
	cout << "nnz:" << nnz << endl;
	cout << "mFaceSize: " << mFaceSize << endl;
	cout << "nowIndex: " << nowIndex << endl;
	cout << "pcvMeasurementsNum: " << pcvMeasurementsNum << endl;

	ret = sparselm_dercrs(OptimizingPCVCost, OptimizingPCVCostAnjacCRS, pcvParams,
		NULL, mFaceSize, 0, pcvMeasurementsNum, nnz, -1, 1000, opts_SPLM, info_SPLM, NULL);

	cout << "ret: " << ret << endl;

	/*Free memory*/
	for (int i = 0; i < mHeight; i++)
	{
		delete[] mPCVs[i];
	}
	delete[] mPCVs;
	delete[] mVectorSH_OPT_HE;


	getchar();

	/*
	//for (int i = 0; i < mHeight; i++)
	//{
	//	for (int j = 0; j < mWidth; j++)
	//	{
	//		if (mImgMark[i][j])
	//		{
	//			cout << mAlbedos[i][j] << endl;
	//		}
	//	}
	//}
	//splab::BriefMat<double> currentMatB(mFaceSize);
	//std::vector<double> currentVectorB;
	//currentVectorB.resize(mFaceSize);
	////Init
	//int** indexRecords = new int*[mHeight];
	//for (int i = 0; i < mHeight; i++)
	//{
	//	indexRecords[i] = new int[mWidth];
	//	for (int j = 0; j < mWidth; j++)
	//	{
	//		indexRecords[i][j] = -1;
	//	}
	//}
	//double** EH = new double*[mHeight];
	//for (int i = 0; i < mHeight; i++)
	//{
	//	EH[i] = new double[mWidth];
	//	for (int j = 0; j < mWidth; j++)
	//	{
	//		EH[i][j] = 0.0f;
	//		if (mImgMark[i][j])
	//		{
	//			double rij = mAlbedos[i][j];
	//			double Iij = mImgIntensity[i][j];
	//			for (int index = 0; index < 9; index++)
	//			{
	//				EH[i][j] += mVectorHE.mE[index] * mVectorSH[i][j].mH[index];
	//			}
	//		}
	//	}
	//}
	//int currentIndex = 0;
	////compute
	//for (int i = 0; i < mHeight; i++)
	//{
	//	for (int j = 0; j < mWidth; j++)
	//	{
	//		if (mImgMark[i][j])
	//		{
	//			double rij = mAlbedos[i][j];
	//			double Iij = mImgIntensity[i][j];
	//			int connectNum = 0;
	//			if (mImgMark[i - 1][j]) //up
	//				connectNum++;
	//			if (mImgMark[i + 1][j]) //down
	//				connectNum++;
	//			if (mImgMark[i][j - 1]) //left
	//				connectNum++;
	//			if (mImgMark[i][j + 1]) //right
	//				connectNum++;
	//			double coefficientA = rij * rij + mWeight.u1 + connectNum * mWeight.u2 + connectNum * connectNum * mWeight.u3;
	//			double coefficientB = (-1) * (mWeight.u2 + connectNum * mWeight.u3);
	//			currentMatB.mValueMat[currentIndex] = coefficientA;
	//			currentMatB.mAttachedValue[currentIndex] = coefficientB;
	//			indexRecords[i][j] = currentIndex;
	//			currentVectorB[currentIndex] = rij * Iij - rij * rij * EH[i][j];
	//			currentIndex++;
	//		}
	//	}
	//}
	//currentIndex = 0;
	//for (int i = 0; i < mHeight; i++)
	//{
	//	for (int j = 0; j < mWidth; j++)
	//	{
	//		if (mImgMark[i][j])
	//		{
	//			int connectNum = 0;
	//			if (indexRecords[i - 1][j] != -1)
	//			{
	//				currentMatB.mConnectMat[currentIndex][connectNum] = indexRecords[i - 1][j];
	//				connectNum++;
	//			}
	//			if (indexRecords[i + 1][j] != -1)
	//			{
	//				currentMatB.mConnectMat[currentIndex][connectNum] = indexRecords[i + 1][j];
	//				connectNum++;
	//			}
	//			if (indexRecords[i][j - 1] != -1)
	//			{
	//				currentMatB.mConnectMat[currentIndex][connectNum] = indexRecords[i][j - 1];
	//				connectNum++;
	//			}
	//			if (indexRecords[i][j + 1] != -1)
	//			{
	//				currentMatB.mConnectMat[currentIndex][connectNum] = indexRecords[i][j + 1];
	//				connectNum++;
	//			}
	//			currentIndex++;
	//		}
	//	}
	//}
	//std::vector<double> resultB = splab::GaussSolverForBriefMat(currentMatB, currentVectorB);
	//cout << "end" << endl;
	//for (int i = 0; i < resultB.size(); i++)
	//{
	//	cout << resultB.size() << endl;
	//}
	//

	//for (int i = 0; i < mHeight; i++)
	//{
	//	for (int j = 0; j < mWidth; j++)
	//	{
	//		if (mImgMark[i][j])
	//		{
	//			if (i == 170 && j == 457)
	//			{
	//				cout << indexRecords[i - 1][j] << "," << indexRecords[i + 1][j] << "," << indexRecords[i][j - 1] << "," << indexRecords[i][j + 1] << endl;
	//			}
	//		}
	//	}
	//}
	////2132	2257  2191  2193;
	//for (int i = 0; i < 4; i++)
	//{
	//	cout << currentBM.mConnectMat[0][i] << ",";
	//}
	//cout << endl;

	//for (int i = 0; i < currentBM.Rows(); i++)
	//{
	//	for (int j = 0; j < currentBM.Cols(); j++)
	//	{
	//		if (i == 0)
	//		{
	//			if (currentBM.ptr(i, j) != 0)
	//			{
	//				cout << j << ":" << currentBM.ptr(i, j) << endl;
	//			}
	//		}
	//	}
	//}

	//for (int i = 0; i < mHeight; i++)
	//{
	//	for (int j = 0; j < mWidth; j++)
	//	{
	//		if (mImgMark[i][j])
	//		{
	//			double rij = mAlbedos[i][j];
	//			double Iij = mImgIntensity[i][j];
	//			double EH = 0;
	//			for (int index = 0; index < 9; index++)
	//			{
	//				EH += mVectorHE.mE[index] * mVectorSH[i][j].mH[index];
	//			}
	//			mPCVs[i][j] = ((rij * Iij) - rij * rij * EH) / (rij * rij + mWeight.u1);
	//			mAlbedos[i][j] = Iij / (EH + mPCVs[i][j]);
	//		}
	//	}
	//}
	*/
}

void FaceSFS::ComputePQ()
{
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mImgMark[i][j] && mImgMark[i + 1][j] && mImgMark[i][j + 1])
			{
				mGradientMark[i][j] = true;
				mPQs[i][j].mP = mDepths[i + 1][j] - mDepths[i][j];
				mPQs[i][j].mQ = mDepths[i][j + 1] - mDepths[i][j];
			}
			else
			{
				mPQs[i][j].mP = 0;
				mPQs[i][j].mQ = 0;
			}
		}
	}
}

void FaceSFS::ComputeImgNormal()
{
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mGradientMark[i][j])
			{
				double p = mPQs[i][j].mP;
				double q = mPQs[i][j].mQ;
				double denominator = NORM_PQ(p, q);
				mImgNormals[i][j][0] = p / denominator;	//x
				mImgNormals[i][j][1] = q / denominator;	//y
				mImgNormals[i][j][2] = -1 / denominator; //z
			}
		}
	}
}

void FaceSFS::ScalingMesh(MyMesh * pM)
{
	double depthDiff = 0.0f;
	for (auto pf : It::MFIterator(pM)) {
		double currentDepth = abs(pf->halfedge()->he_next()->vertex()->point()[2] -
			pf->halfedge()->vertex()->point()[2]);
		if (currentDepth > depthDiff)
		{
			depthDiff = currentDepth;
		}
	}
	double scale = SFS_SCALING_FACTOR / depthDiff;
	for (auto pV : It::MVIterator(pM)) {
		pV->point() *= scale;
	}
}

double FaceSFS::ComputeEH(IntegralPQ& currentPQ)
{
	double nx = currentPQ.mP;
	double ny = currentPQ.mQ;
	double nz = -1;
	double sqrtValue = NORM_PQ(nx, ny);
	nx /= sqrtValue;
	ny /= sqrtValue;
	nz /= sqrtValue;
	double returnValue = mVectorHE.mE[0] + mVectorHE.mE[1] * nx + mVectorHE.mE[2] * ny
		+ mVectorHE.mE[3] * nz + mVectorHE.mE[4] * nx * ny + mVectorHE.mE[5] * nx * nz
		+ mVectorHE.mE[6] * ny * nz + mVectorHE.mE[7] * (nx * nx - ny * ny)
		+ mVectorHE.mE[8] * (3 * nz * nz - 1);
	return returnValue;
}

void FaceSFS::OptimizingPQ()
{
	double opts[SPLM_OPTS_SZ], info[SPLM_INFO_SZ];
	opts[0] = SPLM_INIT_MU;
	opts[1] = SPLM_STOP_THRESH;
	opts[2] = SPLM_STOP_THRESH;
	opts[3] = SPLM_STOP_THRESH;
	opts[4] = SPLM_DIFF_DELTA;
	opts[5] = SPLM_CHOLMOD;

	int paramNum1 = mFaceSize;
	int paramNum2 = mFaceSize * 2;
	int measureNum = 0;
	int nnz = 0;
	int ret = -1;

	//0->no connect 1->connect bottom(i+1) 2->connect right(j+1) 3->connect both(i+1,j+1)
	ifZero_OPT_ENERGY.resize(paramNum2);
	double* params = new double[paramNum2];
	paramIij_OPT_ENERGY = new CPoint2[paramNum1];
	paramRij_OPT_ENERGY = new CPoint[paramNum1];
	paramNormij_OPT_ENERGY = new CPoint[paramNum1];

	connectIndex_OPT_ENERGY = new int*[paramNum1];
	for (int i = 0; i < paramNum1; i++)
	{
		connectIndex_OPT_ENERGY[i] = new int[2];
	}

	int** connectMap = new int*[mHeight];
	for (int i = 0; i < mHeight; i++)
	{
		connectMap[i] = new int[mWidth];
		for (int j = 0; j < mWidth; j++)
			connectMap[i][j] = -1;
	}

	int nowIndex = 0;

	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceOPTMark[i][j])
			{
				int pIndex = 2 * nowIndex;
				int qIndex = pIndex + 1;

				params[pIndex] = mPQs[i][j].mP;
				params[qIndex] = mPQs[i][j].mQ;

				connectMap[i][j] = pIndex;

				double currentEH = ComputeEH(mPQs[i][j]);
				if (currentEH > 0) {
					ifZero_OPT_ENERGY[pIndex] = false;
					ifZero_OPT_ENERGY[qIndex] = false;
				}
				else {
					ifZero_OPT_ENERGY[pIndex] = true;
					ifZero_OPT_ENERGY[qIndex] = true;
				}
				nowIndex++;
			}
		}
	}

	nowIndex = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceOPTMark[i][j])
			{
				paramRij_OPT_ENERGY[nowIndex][0] = mAlbedos[i][j];
				paramRij_OPT_ENERGY[nowIndex][1] = mAlbedos[i + 1][j];
				paramRij_OPT_ENERGY[nowIndex][2] = mAlbedos[i][j + 1];

				paramIij_OPT_ENERGY[nowIndex][0] = 0 - mImgIntensity[i][j];
				paramIij_OPT_ENERGY[nowIndex][1] = 0 - mImgIntensity[i][j];

				connectIndex_OPT_ENERGY[nowIndex][0] = connectMap[i + 1][j];
				connectIndex_OPT_ENERGY[nowIndex][1] = connectMap[i][j + 1];

				if (connectIndex_OPT_ENERGY[nowIndex][0] != -1 && connectIndex_OPT_ENERGY[nowIndex][1] != -1)
					ParamIndexs_OPT_ENERGY.push_back(nowIndex);

				nowIndex++;
			}
		}
	}

	nowIndex = 0;
	int ParamIndexs_OPT_ENERGY_Index = 0;
	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceOPTMark[i][j])
			{
				if (nowIndex == ParamIndexs_OPT_ENERGY[ParamIndexs_OPT_ENERGY_Index])
				{
					/*f(pij, qij, paj, qaj) = (Saj - Sij)*/
					measureNum++;
					nnz += 4;
					paramIij_OPT_ENERGY[nowIndex][0] = mImgIntensity[i + 1][j] - mImgIntensity[i][j];
					/*f(pij, qij, pib, qib) = (Sib - Sij)*/
					measureNum++;
					nnz += 4;
					paramIij_OPT_ENERGY[nowIndex][1] = mImgIntensity[i][j + 1] - mImgIntensity[i][j];
					/*f(pij, qij) = pij / sqrt(pij^2 + qij^2 + 1)*/
					measureNum++;
					nnz += 2;
					paramNormij_OPT_ENERGY[nowIndex][0] = mMeshNormals[i][j][0];
					/*f(pij, qij) = qij / sqrt(pij^2 + qij^2 + 1)*/
					measureNum++;
					nnz += 2;
					paramNormij_OPT_ENERGY[nowIndex][1] = mMeshNormals[i][j][1];
					/*f(pij, qij) = (-1) / sqrt(pij^2 + qij^2 + 1)*/
					measureNum++;
					nnz += 2;
					paramNormij_OPT_ENERGY[nowIndex][2] = mMeshNormals[i][j][2];
					/*f(pij, qij, paj, qaj) = (paj / sqrt(paj^2 + qaj^2 + 1) - pij / sqrt(pij^2 + qij^2 +1))*/
					measureNum++;
					nnz += 4;
					/*f(pij, qij, paj, qaj) = (qaj / sqrt(paj^2 + qaj^2 + 1) - qij / sqrt(pij^2 + qij^2 +1))*/
					measureNum++;
					nnz += 4;
					/*f(pij, qij, paj, qaj) = (-1 / sqrt(paj^2 + qaj^2 + 1) + 1 / sqrt(pij^2 + qij^2 +1))*/
					measureNum++;
					nnz += 4;
					/*f(pij, qij, pib, qib) = (pib / sqrt(pib^2 + qib^2 + 1) - pij / sqrt(pij^2 + qij^2 +1))*/
					measureNum++;
					nnz += 4;
					/*f(pij, qij, pib, qib) = (qib / sqrt(pib^2 + qib^2 + 1) - qij / sqrt(pij^2 + qij^2 +1))*/
					measureNum++;
					nnz += 4;
					/*f(pij, qij, pib, qib) = (-1 / sqrt(pib^2 + qib^2 + 1) + 1 / sqrt(pij^2 + qij^2 +1))*/
					measureNum++;
					nnz += 4;
					///*f(pij, qij, qaj, pib) = (pij - qij + qaj - pib)*/
					//measureNum++;
					//nnz += 4;

					ParamIndexs_OPT_ENERGY_Index++;
				}
				nowIndex++;
			}
		}
	}
	for (int i = 0; i < 9; i++)
		functionHE_OPT_ENERGY.mE[i] = mVectorHE.mE[i];

	cout << "paramNum2: " << paramNum2 << endl;
	cout << "nnz: " << nnz << endl;
	cout << "ParamIndexs_OPT_ENERGY_Index" << ParamIndexs_OPT_ENERGY_Index << endl;
	cout << "measureNum" << measureNum << endl;

	ret = sparselm_dercrs(OptimizingCost, OptimizingCostAnjacCRS, params, NULL, paramNum2, 0, measureNum, nnz, -1, 1000, opts, info, NULL);
	printf("iterations:\t%d\n", ret);

	nowIndex = 0;

	for (int i = 0; i < mHeight; i++)
	{
		for (int j = 0; j < mWidth; j++)
		{
			if (mFaceOPTMark[i][j])
			{
				int pIndex = 2 * nowIndex;
				int qIndex = pIndex + 1;

				mPQs[i][j].mP = params[pIndex];
				mPQs[i][j].mQ = params[qIndex];
				nowIndex++;
			}
		}
	}
}

/*
//long long imageSize = mHeight * mWidth;
//MyMat currentMatB(imageSize, imageSize);
//std::vector<double> currentVectorB;
//currentVectorB.resize(imageSize);
//cout << imageSize << endl;
//for (int i = 0; i < imageSize; i++)
//{
//	for (int j = 0; j < imageSize; j++)
//	{
//		currentMatB.ptr(i, j) = 0.0001f;
//	}
//}
//for (int i = 0; i < mHeight; i++)
//{
//	for (int j = 0; j < mWidth; j++)
//	{
//		if (mImgMark[i][j])
//		{
//			double rij = mAlbedos[i][j];
//			int matBIndex_Center = i * mWidth + j;
//			currentMatB.ptr(matBIndex_Center, matBIndex_Center)
//				= (rij * rij + mWeight.u1 + mWeight.u2 + 16 * mWeight.u3);
//			double coefficientB = (-1) * (mWeight.u2 + 4 * mWeight.u3);
//			currentMatB.ptr(matBIndex_Center, matBIndex_Center - mWidth) = coefficientB;
//			currentMatB.ptr(matBIndex_Center, matBIndex_Center + mWidth) = coefficientB;
//			currentMatB.ptr(matBIndex_Center, matBIndex_Center - 1) = coefficientB;
//			currentMatB.ptr(matBIndex_Center, matBIndex_Center + 1) = coefficientB;
//
//			double Iij = mImgIntensity[i][j];
//			double EH = (mVectorHE.mE[0] * mVectorSH[i][j].mH[0] + mVectorHE.mE[1] * mVectorSH[i][j].mH[1] +
//				mVectorHE.mE[2] * mVectorSH[i][j].mH[2] + mVectorHE.mE[3] * mVectorSH[i][j].mH[3] +
//				mVectorHE.mE[4] * mVectorSH[i][j].mH[4] + mVectorHE.mE[5] * mVectorSH[i][j].mH[5] +
//				mVectorHE.mE[6] * mVectorSH[i][j].mH[6] + mVectorHE.mE[7] * mVectorSH[i][j].mH[7] +
//				mVectorHE.mE[8] * mVectorSH[i][j].mH[8]);
//			currentVectorB[matBIndex_Center] = (rij * rij * EH - rij * Iij);
//		}
//		else
//			currentVectorB[i * mWidth + j] = 0.0001f;
//	}
//}
//std::vector<double> resultB = splab::gaussSolver(currentMatB, currentVectorB);
//for (int i = 0; i < mHeight; i++)
//{
//	for (int j = 0; j < mWidth; j++)
//	{
//		int index = i * mWidth + j;
//		mAlbedos[i][j] = resultB[index];
//		cout << mAlbedos[i][j] << endl;
//	}
//}

*/