#ifndef FACESFS_H
#define FACESFS_H

//Other Library
#include <iostream>
#include <process.h>
#include <math.h>
//OpenCV Library
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
//PCL Library
//#include <pcl/io/pcd_io.h>
//#include <pcl/point_types.h>
//MeshLib Library
#include <MeshLib/core/Mesh/MeshCoreHeaders.h>
//MyMath Library
#include "linequs.h"
#include <splm.h>
#include "levmar.h"


#define SFS_SCALING_FACTOR 100.0f

using namespace MeshLib;
using namespace std;

typedef CBaseMesh<CVertexUV, CEdge, CFace, CHalfEdge> MyMesh;
typedef CIterators<MyMesh> It;

struct IntegralPQ
{
	double mP = 0;
	double mQ = 0;
};

struct VectorSH
{
	double mH[9] = { 0 };
};

struct VectorHE
{
	double mE[9] = { 0 };
};

struct WeightSFS
{
	double u1 = 0;
	double u2 = 0;
	double u3 = 0;
};

class FaceSFS
{
public:
	typedef MeshLib::CPoint SFSPoint;
	typedef MeshLib::CPoint2 SFSPoint2;
	typedef splab::MyMat<double>	MyMat;

public:
	FaceSFS(cv::Mat rgbImage, MyMesh* myMesh, WeightSFS myWeight);
	~FaceSFS();
	void ShapeFromShading();

private:
	/*Find the face range and compute the mesh's normal in pixels range*/
	void FindFace_ComputeNormal_GetDepth();
	/*Compute the albedos of input gray image*/
	void ComputeAlbedos();
	/*Compute the gradient */
	void ComputePQ();
	/*Compute the normal*/
	void ComputeImgNormal();
	/*Optimizing the p & q*/
	void OptimizingPQ();
	/*Compute the EH*/
	double ComputeEH(IntegralPQ& currentPQ);

private:
	void SelectingRange(int startRow, int endRow, int startCol, int endCol);
	void ScalingMesh(MyMesh*);
	void RefineDepth();

private:
	/*Input image's size*/
	int mHeight;
	int mWidth;
	/*The formular's weight in computing albedos*/
	WeightSFS mWeight;
	/*Input mesh*/
	MyMesh* mMesh;
	/*Input gray image*/
	cv::Mat mGrayImg;
	/*Face Image mark*/
	bool** mImgMark;
	/*The size of face pixels*/
	int mFaceSize;
	/*Mesh's normals*/
	SFSPoint** mMeshNormals;
	/*The gray image's intensity(pixels value)*/
	double** mImgIntensity;
	/*The face object's albedos*/
	double** mAlbedos;
	/*The harmonic coefficients*/
	VectorHE mVectorHE;
	/*The depth(value(ij)) information from mesh(depth image)*/
	double** mDepths;
	/*Gradient mark*/
	bool** mGradientMark;
	/*The gradient of depth*/
	IntegralPQ** mPQs;
	/*The image normal*/
	SFSPoint** mImgNormals;
	/*Face optimization mark*/
	bool** mFaceOPTMark;

	///////////////////////
	cv::Mat resultShowThree;
	cv::Mat resultShowOne;
};

#endif